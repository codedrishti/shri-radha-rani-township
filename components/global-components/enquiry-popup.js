import React, { useState, useRef } from "react";
import Loader from "../loader";

export default function EnquiryPopup() {
     const ref = useRef(null)

     const [form, setForm] = useState({
          name: "",
          email: "",
          phone: "",
          address: "",
     });

     const [isSubmitting, setIsSubmitting] = useState(false);
     const [errors, setErrors] = useState({});

     const handleInputs = (e) => {
          const name = e.target.name;
          const value = e.target.value;

          setForm({
               ...form,
               [name]: value,
          });
     };

     const handleSubmit = async (e) => {
          e.preventDefault();

          const { name, email, phone, address } = form;

          setIsSubmitting(true);

          try {
               const res = await fetch("/api/enquiry", {
                    method: "POST",
                    headers: {
                         Accept: "application/json, text/plain, */*",
                         "Content-Type": "application/json",
                    },
                    body: JSON.stringify(form),
               });

               const result = await res.json();

               if (res.status === 201) {
                    window.alert("Data submitted successfully!");
                    setForm({});
                    setIsSubmitting(false);
                    ref.current.click();
               }
          } catch (err) {
               console.log(err);
          }
     };

     return (
          <>
               {/* // Enquiry Form Modal */}
               <div className="modal fade" id="enquiryPopup" tabIndex={-1} role="dialog">
                    <div className="modal-dialog modal-md" role="document">
                         <div className="modal-content">
                              {isSubmitting ? (
                                   <Loader />
                              ) : (
                                   <form onSubmit={handleSubmit}>
                                        <div className="modal-header">
                                             <h5 className="modal-title">BASIC INFORMATION</h5>
                                             <button
                                                  type="button"
                                                  className="close"
                                                  data-bs-dismiss="modal"
                                                  aria-label="Close"
                                                  ref={ref}
                                             >
                                                  <span aria-hidden="true">×</span>
                                             </button>
                                        </div>

                                        <div className="modal-body">
                                             <div className="form-group">
                                                  <input
                                                       type="text"
                                                       className="form-control"
                                                       name="name"
                                                       placeholder="Name"
                                                       value={form.name}
                                                       onChange={handleInputs}
                                                       required={true}
                                                  />
                                             </div>
                                             <div className="form-group">
                                                  <input
                                                       type="tel"
                                                       className="form-control"
                                                       name="phone"
                                                       placeholder="Phone"
                                                       value={form.phone}
                                                       onChange={handleInputs}
                                                       required={true}
                                                  />
                                             </div>
                                             <div className="form-group">
                                                  <input
                                                       type="email"
                                                       className="form-control"
                                                       name="email"
                                                       placeholder="Email"
                                                       value={form.email}
                                                       onChange={handleInputs}
                                                       required={true}
                                                  />
                                             </div>
                                             <div className="form-group">
                                                  <div className="form-check">
                                                       <input
                                                            className="form-check-input"
                                                            type="checkbox"
                                                            id="ppIvisit"
                                                            checked={true}
                                                            required={true}
                                                       />
                                                       <label className="form-check-label" htmlFor="gridCheck">
                                                            I am interested in site visits.
                                                       </label>
                                                  </div>
                                             </div>
                                             <div className="form-group">
                                                  <div className="form-check">
                                                       <input
                                                            className="form-check-input"
                                                            type="checkbox"
                                                            id="ppTnC"
                                                            checked={true}
                                                            required={true}
                                                       />
                                                       <label className="form-check-label" htmlFor="gridCheck">
                                                            I agree to be contacted by shriradhanitownship.com for
                                                            properties or related services via WhatsApp, phone, sms,
                                                            e-mail etc.
                                                       </label>
                                                  </div>
                                             </div>
                                        </div>
                                        <div className="modal-footer">
                                             <button type="submit" className="theme-btn-1 btn btn-effect-2 btn-sm">
                                                  <i className="fa fa-heart" />
                                                  &nbsp; Submit
                                             </button>
                                        </div>
                                   </form>
                              )}
                         </div>
                    </div>
               </div>

               {/* // Become Associate Form Modal */}
               <div className="modal fade" id="associate_Enquiry_Form" tabIndex={-1} role="dialog">
                    <div className="modal-dialog modal-md" role="document">
                         <div className="modal-content">
                              {isSubmitting ? (
                                   <Loader />
                              ) : (
                                   <form onSubmit={handleSubmit}>
                                        <div className="modal-header">
                                             <h5 className="modal-title">BECOME AN ASSOCIATE</h5>
                                             <button
                                                  type="button"
                                                  className="close"
                                                  data-bs-dismiss="modal"
                                                  aria-label="Close"
                                                  ref={ref}
                                             >
                                                  <span aria-hidden="true">×</span>
                                             </button>
                                        </div>
                                        <div className="modal-body">
                                             <div className="form-group">
                                                  <input
                                                       type="text"
                                                       className="form-control"
                                                       name="name"
                                                       placeholder="Name"
                                                       value={form.name}
                                                       onChange={handleInputs}
                                                       required
                                                  />
                                             </div>
                                             <div className="form-group">
                                                  <input
                                                       type="tel"
                                                       className="form-control"
                                                       name="phone"
                                                       placeholder="Phone"
                                                       value={form.phone}
                                                       onChange={handleInputs}
                                                       required
                                                  />
                                             </div>
                                             <div className="form-group">
                                                  <input
                                                       type="email"
                                                       className="form-control"
                                                       name="email"
                                                       placeholder="Email"
                                                       value={form.email}
                                                       onChange={handleInputs}
                                                       required
                                                  />
                                             </div>
                                             <div className="form-group">
                                                  <input
                                                       type="text"
                                                       className="form-control"
                                                       name="address"
                                                       placeholder="Address"
                                                       value={form.address}
                                                       onChange={handleInputs}
                                                       required
                                                  />
                                             </div>
                                             <div className="form-group">
                                                  <div className="form-check">
                                                       <input
                                                            className="form-check-input"
                                                            type="checkbox"
                                                            id="ppIvisit"
                                                            checked={true}
                                                       />
                                                       <label className="form-check-label" htmlFor="gridCheck">
                                                            I am interested to associate with Shri Radha Rani Township.
                                                       </label>
                                                  </div>
                                             </div>
                                             <div className="form-group">
                                                  <div className="form-check">
                                                       <input
                                                            className="form-check-input"
                                                            type="checkbox"
                                                            id="ppTnC"
                                                            checked={true}
                                                       />
                                                       <label className="form-check-label" htmlFor="gridCheck">
                                                            I agree to be contacted by shriradhanitownship.com for
                                                            properties or related services via WhatsApp, phone, sms,
                                                            e-mail etc.
                                                       </label>
                                                  </div>
                                             </div>
                                        </div>
                                        <div className="modal-footer">
                                             <button type="submit" className="theme-btn-1 btn btn-effect-2 btn-sm">
                                                  <i className="fa fa-heart" />
                                                  &nbsp; Submit
                                             </button>
                                        </div>
                                   </form>
                              )}
                         </div>
                    </div>
               </div>
          </>
     );
}
