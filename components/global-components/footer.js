import React, { Component } from "react";
import Link from "next/link";
import $ from "jquery";
// import Social from '../section-components/social';
// import Copyright from "./copyright";
import EnquiryPopup from "../global-components/enquiry-popup";

class Footer_v1 extends Component {
     componentDidMount() {
          $(".go-top")
               .find("a")
               .on("click", function () {
                    $(".quarter-overlay").fadeIn(1);

                    $(window).scrollTop(0);

                    setTimeout(function () {
                         $(".quarter-overlay").fadeOut(300);
                    }, 800);
               });

          // $(document).on("click", ".theme-btn-1 ", function () {
          //      $("div").removeClass("modal-backdrop");
          //      $("div").removeClass("show");
          //      $("div").removeClass("fade");
          //      $("body").attr("style", "");
          // });
     }

     render() {
          let publicUrl = "/";
          let imgattr = "Footer logo";

          return (
               <footer className="ltn__footer-area  ">
                    <div className="footer-top-area  section-bg-2 plr--5">
                         <div className="container-fluid">
                              <div className="row">
                                   <div className="col-xl-5 col-md-6 col-sm-6 col-12">
                                        <div className="footer-widget footer-about-widget">
                                             <div className="footer-logo">
                                                  <div className="site-logo">
                                                       <img src={publicUrl + "assets/img/logo-2.png"} alt="Logo" />
                                                  </div>
                                             </div>
                                             <p>
                                                  Property developers are in a position to make a significant impact on
                                                  society by building and developing better places for people to live.
                                                  KRS Home Developers is a property developer that is involved with many
                                                  facets in building better communities. The company is involved in
                                                  constructing, developing, and marketing land. KRS Home Developers is a
                                                  property developer in India. They provide affordable properties to the
                                                  people of India and make them world-class. We have a vision to make a
                                                  world-class society in its own right. They have become one of the most
                                                  successful property developers of the India. They provide affordable
                                                  properties to the middle-class families in their budget.
                                             </p>
                                        </div>
                                   </div>
                                   <div className="col-xl-2 col-md-6 col-sm-6 col-12">
                                        <div className="footer-widget footer-menu-widget clearfix">
                                             <h4 className="footer-title">Company</h4>
                                             <div className="footer-menu go-top">
                                                  <ul>
                                                       <li>
                                                            <a href="index">Home</a>
                                                       </li>
                                                       <li>
                                                            <a href="about">About</a>
                                                       </li>
                                                       <li>
                                                            <a href="projects">Projects</a>
                                                       </li>
                                                       <li>
                                                            <a href="faq">FAQ</a>
                                                       </li>
                                                       <li>
                                                            <a href="contact">Contact</a>
                                                       </li>
                                                       <li>
                                                            <a href="company/terms-and-conditions">Terms & Conditions</a>
                                                       </li>
                                                       <li>
                                                            <a href="company/privacy-policy">Privacy Policy</a>
                                                       </li>
                                                  </ul>
                                             </div>
                                        </div>
                                   </div>

                                   <div className="col-xl-2 col-md-6 col-sm-6 col-12">
                                        <div className="footer-widget footer-menu-widget clearfix">
                                             <h4 className="footer-title">Projects</h4>
                                             <div className="footer-menu go-top">
                                                  <ul>
                                                       <li>
                                                            <Link href="brij-rani-kuteer-township-vrindavan">Brij Rani Kuteer Vrindavan</Link>
                                                       </li>
                                                       <li>
                                                            <Link href="shri-radha-rani-township-barsana">Shri Radha Rani Township Barsana</Link>
                                                       </li>
                                                       <li>
                                                            <Link href="shri-radha-rani-township-phase-1">Shri Radha Rani Township Phase I</Link>
                                                       </li>
                                                       <li>
                                                            <Link href="shri-radha-rani-township-barsana-govardhan">Shri Radha Rani Township Govardhan</Link>
                                                       </li>
                                                       <li>
                                                            <Link href="maa-kaila-devi-township-agra">Maa Kaila Devi Township Agra</Link>
                                                       </li>
                                                  </ul>
                                             </div>
                                        </div>
                                   </div>
                                   <div className="col-xl-3 col-md-6 col-sm-12 col-12">
                                        <div className="footer-widget footer-newsletter-widget">
                                             <h4 className="footer-title">Contact</h4>
                                             <p>You reach us for any information related to us.</p>
                                             <div className="footer-address">
                                                  <ul>
                                                       <li>
                                                            <div className="footer-address-icon">
                                                                 <i className="icon-placeholder" />
                                                            </div>
                                                            <div className="footer-address-info">
                                                                 <p>15/1, Main Mathura Road, Faridabad</p>
                                                            </div>
                                                       </li>
                                                       <li>
                                                            <div className="footer-address-icon">
                                                                 <i className="icon-call" />
                                                            </div>
                                                            <div className="footer-address-info">
                                                                 <p>
                                                                      <a href="tel:07550400495">07550-400-495</a>
                                                                 </p>
                                                            </div>
                                                       </li>
                                                       <li>
                                                            <div className="footer-address-icon">
                                                                 <i className="icon-mail" />
                                                            </div>
                                                            <div className="footer-address-info">
                                                                 <p>
                                                                      <a href="mailto:info@shriradharanitownship.com">
                                                                           info@shriradharanitownship.com
                                                                      </a>
                                                                 </p>
                                                            </div>
                                                       </li>
                                                  </ul>
                                             </div>
                                             <div className="ltn__social-media mt-20">
                                                  {/* <Social /> */}
                                                  <ul>
                                                       <li>
                                                            <a
                                                                 href="https://www.facebook.com/shriradharanitownship"
                                                                 title="Facebook"
                                                            >
                                                                 <i className="fab fa-facebook-f"></i>
                                                            </a>
                                                       </li>
                                                       <li>
                                                            <a href="https://twitter.com/srrtownship" title="Twitter">
                                                                 <i className="fab fa-twitter"></i>
                                                            </a>
                                                       </li>
                                                       <li>
                                                            <a
                                                                 href="https://www.instagram.com/krsgroupofficial"
                                                                 title="Instagram"
                                                            >
                                                                 <i className="fab fa-instagram"></i>
                                                            </a>
                                                       </li>
                                                       <li>
                                                            <a
                                                                 href="https://www.youtube.com/channel/UClpsbjD1QeinYXk2ZNSBGQw"
                                                                 title="YouTube"
                                                            >
                                                                 <i className="fab fa-youtube"></i>
                                                            </a>
                                                       </li>
                                                  </ul>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
                    {/* <Copyright /> */}
                    <EnquiryPopup/>
               </footer>
          );
     }
}

export default Footer_v1;
