import React, { Component } from "react";
import Link from "next/link";

function Page_header({ headertitle, customclass, sublink }) {
     let HeaderTitle = headertitle;
     let publicUrl = "/";
     let Subheader = headertitle ? headertitle : HeaderTitle;
     let CustomClass = customclass ? customclass : "";
     //let Img = this.props.Img ? this.props.Img : "14.jpg";

     return (
          <div
               className={"ltn__breadcrumb-area text-left bg-image " + CustomClass}
               style={{ paddingTop: "0", paddingBottom: "0", background: "#ff5a3c" }}
          >
               <div className="container">
                    <div className="row">
                         <div className="col-lg-12">
                              <div className="ltn__breadcrumb-inner">
                                   {/* <h1 className="page-title">{HeaderTitle}</h1> */}
                                   <div className="ltn__breadcrumb-list">
                                        <ul>
                                             <li>
                                                  <a href="/">Home</a>
                                             </li>
                                             <li>
                                                  <a href={sublink}>{Subheader}</a>
                                             </li>
                                        </ul>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     );
}

export default Page_header;
