import React from "react";
import Head from "next/head";

export default function SEO({
     description = "Shri Radha Rani Township is a gated society developed by KRS Group developer in Mathura, Barsana, Vrindavan, Govardhan and Agra. We provide affordable properties to the people of India and make them world-class. We have a vision to make a world-class society in its own right.",
     author = "KRS Group",
     meta,
     title = "Shriradharanitownship.com Official Site - Best Gated Society With Top World-Class Aminities",
}) {
     const metaData = [
          {
               name: `robots`,
               content: `index,follow`
          },
          {
               name: `googlebot`,
               content: `index,follow`
          },
          {
               name: `description`,
               content: description,
          },
          {
               property: `og:title`,
               content: title,
          },
          {
               property: `og:description`,
               content: title,
          },
          {
               property: `og:type`,
               content: `website`,
          },
          {
               name: `og:image`,
               content: `https://shriradharanitownship.com/assets/img/logo.png`
          },
          {
               name: `og:local`,
               content: `en_IN`,
          },
          {
               name: `og:site_name`,
               content: `Shri Radha Rani Township | Official Website`,
          },
          {
               name: `twitter:card`,
               content: `summary`,
          },
          {
               name: `twitter:creator`,
               content: author,
          },
          {
               name: `twitter:title`,
               content: title,
          },
          {
               name: `twitter:description`,
               content: description,
          },
     ].concat(meta);
     return (
          <Head>
               <title>{title}</title>
               <link rel="icon" type="image/jpg" href="favicon.png"/>
               {metaData.map(({ name, content }, i) => (
                    <meta key={i} name={name} content={content} />
               ))}
               <link rel="canonical" href="https://shriradharanitownship.com/"/>
          </Head>
     );
}

SEO.defaultProps = {
     lang: `en`,
     meta: [],
};
