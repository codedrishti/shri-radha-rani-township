import React, { useState, useEffect } from "react";
import Link from "next/link";
import parse from "html-react-parser";
const allProjectData = require("../../controller/ProjectDetails.json");

function ProductSliderV1({projectSlider}) {
     let publicUrl = "/";

     // const [projectSlider, setProjectSlider] = useState([]);



     // let projectSlider;

     // useEffect(() => {
     //      projectSlider = allProjectData.filter(
     //           (projectData) => projectData.permaLink === projectPermalink
     //      );
     //      setProjectDetails(projectSlider[0].slider);
     // }, []);

     return (
          <div className="ltn__img-slider-area mb-90">
               <div className="container-fluid">
                    <div className="row ltn__image-slider-5-active slick-arrow-1 slick-arrow-1-inner ltn__no-gutter-all">
                         {projectSlider.map((curElem, i) => (
                              <div className="col-lg-12" key={i}>
                                   <div className="ltn__img-slide-item-4">
                                        <a href={curElem} data-rel="lightcase:myCollection">
                                             <img src={curElem} alt="Image" />
                                        </a>
                                   </div>
                              </div>
                         ))}
                    </div>
               </div>
          </div>
     );
}

export default ProductSliderV1;
