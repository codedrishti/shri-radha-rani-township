import React, { useState, useEffect } from "react";
import Link from "next/link";
import parse from "html-react-parser";
import CallToActon from "../section-components/call-to-action-v4";
import Loader from "../loader";
const allProjectData = require("../../controller/ProjectDetails.json");

function ShopDetails({ projectDetails }) {
     //console.log(projectPermalink);
     let publicUrl = "/";

     const [form, setForm] = useState({
          name: "",
          email: "",
          phone: "",
          message: "",
     });
     const [isSubmit, setIsSubmit] = useState(false);

     const [projectInfo, setProjectInfo] = useState([]);
     const [saleBadg, setSaleBadg] = useState([]);
     const [propertyDetailsLft, setPropertyDetailsLft] = useState([]);
     const [propertyDetailsRgt, setPropertyDetailsRgt] = useState([]);
     const [gallery, setGallery] = useState([]);
     const [facilities, setFacilities] = useState([]);
     const [amenities, setAmenities] = useState([]);
     const [propertyVideo, setPropertyVideo] = useState([]);
     const [author, setAuthor] = useState([]);

     useEffect(() => {
          // projectDetails = allProjectData.filter(
          //      (projectData) => projectData.permaLink === projectPermalink
          // );

          setProjectInfo(projectDetails);
          setSaleBadg(projectDetails.saleBadg);
          setPropertyDetailsLft(projectDetails.propertyDetailsLft);
          setPropertyDetailsRgt(projectDetails.propertyDetailsRgt);
          setGallery(projectDetails.gallery);
          setFacilities(projectDetails.facilities);
          setAmenities(projectDetails.amenities);
          setPropertyVideo(projectDetails.propertyVideo);
          setAuthor(projectDetails.author);
     }, []);

     const handleInputs = (e) => {
          const name = e.target.name;
          const value = e.target.value;

          setForm({
               ...form,
               [name]: value,
          });
     };

     const handleEnquiry = async (e) => {
          e.preventDefault();

          const { name, email, phone, message } = form;

          setIsSubmit(true);

          try {
               const res = await fetch("/api/enquiry/projapi", {
                    method: "POST",
                    headers: {
                         Accept: "application/json, text/plain, */*",
                         "Content-Type": "application/json",
                    },
                    body: JSON.stringify(form),
               });

               const result = await res.json();

               if (res.status === 201) {
                    window.alert("Data submitted successfully!");
                    setForm({});
                    setIsSubmit(false);
               }
          } catch (err) {
               console.log(err);
          }
     };

     return (
          <>
               <div className="ltn__shop-details-area pb-10">
                    <div className="container">
                         <div className="row">
                              <div className="col-lg-8 col-md-12">
                                   <div className="ltn__shop-details-inner ltn__page-details-inner mb-60">
                                        <div className="ltn__blog-meta">
                                             <ul>
                                                  {saleBadg.map((curBadg, i) => (
                                                       <li className="ltn__blog-category" key={i}>
                                                            <Link href="#">{curBadg}</Link>
                                                       </li>
                                                  ))}
                                             </ul>
                                        </div>
                                        <h1>{projectInfo.projectName}</h1>
                                        <label>
                                             <span className="ltn__secondary-color">
                                                  <i className="flaticon-pin" />
                                             </span>{" "}
                                             {projectInfo.location}
                                        </label>
                                        <h4 className="title-2">Description</h4>

                                        <p dangerouslySetInnerHTML={{ __html: projectInfo.description }} />

                                        <h4 className="title-2">Property Detail</h4>
                                        <div className="property-detail-info-list section-bg-1 clearfix mb-60">
                                             <ul>
                                                  <li>
                                                       <label>Property ID:</label>{" "}
                                                       <span>{propertyDetailsLft.propertyId}</span>
                                                  </li>
                                                  <li>
                                                       <label>Home Area: </label>{" "}
                                                       <span>{propertyDetailsLft.projArea}</span>
                                                  </li>
                                                  <li>
                                                       <label>Plot Size:</label>{" "}
                                                       <span>{propertyDetailsLft.plotSize}</span>
                                                  </li>
                                                  <li>
                                                       <label>Road:</label> <span>{propertyDetailsLft.road}</span>
                                                  </li>
                                             </ul>
                                             <ul>
                                                  <li>
                                                       <label>Park Size:</label>{" "}
                                                       <span>{propertyDetailsRgt.parkSize} </span>
                                                  </li>
                                                  <li>
                                                       <label>Plot Cost:</label>{" "}
                                                       <span>{propertyDetailsRgt.plotCost}</span>
                                                  </li>
                                                  <li>
                                                       <label>Inventory:</label>{" "}
                                                       <span>{propertyDetailsRgt.inventory}</span>
                                                  </li>
                                                  <li>
                                                       <label>Property Status:</label> <span>For Sale</span>
                                                  </li>
                                             </ul>
                                        </div>
                                        <h4 className="title-2">Society Facilities</h4>
                                        <div className="property-detail-feature-list clearfix mb-45">
                                             <ul>
                                                  {facilities.map((curElem, i) => (
                                                       <li key={i}>
                                                            <div className="property-detail-feature-list-item">
                                                                 <i className={curElem.icon} />
                                                                 <div>
                                                                      <h6>{curElem.name}</h6>
                                                                      {/* <small>20 x 16 sq feet</small> */}
                                                                 </div>
                                                            </div>
                                                       </li>
                                                  ))}
                                             </ul>
                                        </div>
                                        <h4 className="title-2">From Our Gallery</h4>
                                        <div className="ltn__property-details-gallery mb-30">
                                             <div className="row">
                                                  <div className="col-md-6">
                                                       <a href={gallery.img1} data-rel="lightcase:myCollection">
                                                            <img className="mb-30" src={gallery.img1} alt="Image" />
                                                       </a>
                                                       <a href={gallery.img2} data-rel="lightcase:myCollection">
                                                            <img className="mb-30" src={gallery.img2} alt="Image" />
                                                       </a>
                                                  </div>
                                                  <div className="col-md-6">
                                                       <a href={gallery.img3} data-rel="lightcase:myCollection">
                                                            <img className="mb-30" src={gallery.img3} alt="Image" />
                                                       </a>
                                                  </div>
                                             </div>
                                        </div>
                                        <h4 className="title-2 mb-10">Amenities</h4>
                                        <div className="property-details-amenities mb-60">
                                             <div className="row">
                                                  {amenities.map((curAmenities, i) => (
                                                       <div className="col-lg-4 col-md-6" key={i}>
                                                            <div className="ltn__menu-widget">
                                                                 <ul>
                                                                      <li key={i}>
                                                                           <label className="checkbox-item">
                                                                                {curAmenities}
                                                                                <input
                                                                                     type="checkbox"
                                                                                     defaultChecked="checked"
                                                                                />
                                                                                <span className="checkmark" />
                                                                           </label>
                                                                      </li>
                                                                 </ul>
                                                            </div>
                                                       </div>
                                                  ))}
                                             </div>
                                        </div>
                                        <h4 className="title-2">Location</h4>
                                        <div className="property-details-google-map mb-60">
                                             <iframe
                                                  src={projectInfo.mapLocation}
                                                  width="100%"
                                                  height="100%"
                                                  frameBorder={0}
                                                  allowFullScreen
                                                  aria-hidden="false"
                                                  tabIndex={0}
                                             />
                                        </div>

                                        {/* CALL TO ACTION & DOWNLOAD BROCHURE */}
                                        <CallToActon />

                                        <h4 className="title-2">Property Video</h4>
                                        <div
                                             className="ltn__video-bg-img ltn__video-popup-height-500 bg-overlay-black-50 bg-image mb-60"
                                             data-bs-bg={propertyVideo.thumbnail}
                                        >
                                             <a
                                                  className="ltn__video-icon-2 ltn__video-icon-2-border---"
                                                  href={propertyVideo.videoLink}
                                                  data-rel="lightcase:myCollection"
                                             >
                                                  <i className="fa fa-play" />
                                             </a>
                                        </div>
                                   </div>
                              </div>
                              <div className="col-lg-4">
                                   <aside className="sidebar ltn__shop-sidebar ltn__right-sidebar---">
                                        {/* Form Widget */}
                                        <div className="widget ltn__form-widget">
                                             <h4 className="ltn__widget-title ltn__widget-title-border-2">
                                                  Get in touch
                                             </h4>
                                             {isSubmit ? (
                                                  <Loader />
                                             ) : (
                                                  <form onSubmit={handleEnquiry}>
                                                       <input
                                                            type="text"
                                                            name="name"
                                                            placeholder="Your Name*"
                                                            value={form.name}
                                                            onChange={handleInputs}
                                                            required={true}
                                                       />
                                                       <input
                                                            type="tel"
                                                            name="phone"
                                                            placeholder="Your Mobile*"
                                                            value={form.phone}
                                                            onChange={handleInputs}
                                                            required={true}
                                                       />
                                                       <input
                                                            type="email"
                                                            name="email"
                                                            placeholder="Your Email*"
                                                            value={form.email}
                                                            onChange={handleInputs}
                                                            required={true}
                                                       />
                                                       <textarea
                                                            name="message"
                                                            placeholder="Write Message..."
                                                            value={form.message}
                                                            onChange={handleInputs}
                                                       />
                                                       <button type="submit" className="btn theme-btn-1">
                                                            Send Messege
                                                       </button>
                                                  </form>
                                             )}
                                        </div>
                                        {/* Author Widget */}
                                        <div className="widget ltn__author-widget">
                                             <div className="ltn__author-widget-inner text-center">
                                                  <img src={author.img} alt="Image" />
                                                  <h5>{author.name}</h5>
                                                  <small>{author.designation}</small>
                                                  <div className="product-ratting">
                                                       <ul>
                                                            <li>
                                                                 <a href="#">
                                                                      <i className="fas fa-star" />
                                                                 </a>
                                                            </li>
                                                            <li>
                                                                 <a href="#">
                                                                      <i className="fas fa-star" />
                                                                 </a>
                                                            </li>
                                                            <li>
                                                                 <a href="#">
                                                                      <i className="fas fa-star" />
                                                                 </a>
                                                            </li>
                                                            <li>
                                                                 <a href="#">
                                                                      <i className="fas fa-star" />
                                                                 </a>
                                                            </li>
                                                            <li>
                                                                 <a href="#">
                                                                      <i className="fas fa-star" />
                                                                 </a>
                                                            </li>
                                                            <li className="review-total">
                                                                 {" "}
                                                                 {/* <a href="#"> ( 1 Reviews )</a> */}
                                                            </li>
                                                       </ul>
                                                  </div>
                                                  <p>{author.description}</p>
                                                  <div className="ltn__social-media">
                                                       <ul>
                                                            <li>
                                                                 <a href="#" title="Facebook">
                                                                      <i className="fab fa-facebook-f" />
                                                                 </a>
                                                            </li>
                                                            <li>
                                                                 <a href="#" title="Twitter">
                                                                      <i className="fab fa-twitter" />
                                                                 </a>
                                                            </li>
                                                            <li>
                                                                 <a href="#" title="Linkedin">
                                                                      <i className="fab fa-linkedin" />
                                                                 </a>
                                                            </li>
                                                            <li>
                                                                 <a href="#" title="Youtube">
                                                                      <i className="fab fa-youtube" />
                                                                 </a>
                                                            </li>
                                                       </ul>
                                                  </div>
                                             </div>
                                        </div>
                                        {/* Search Widget */}
                                        {/* <div className="widget ltn__search-widget">
                                        <h4 className="ltn__widget-title ltn__widget-title-border-2">Search Objects</h4>
                                        <form action="#">
                                             <input type="text" name="search" placeholder="Search your keyword..." />
                                             <button type="submit">
                                                  <i className="fas fa-search" />
                                             </button>
                                        </form>
                                   </div> */}

                                        {/* Menu Widget (Category) */}
                                        <div className="widget ltn__menu-widget ltn__menu-widget-2--- ltn__menu-widget-2-color-2---">
                                             <h4 className="ltn__widget-title ltn__widget-title-border-2">
                                                  Top Properties
                                             </h4>
                                             <ul className="go-top">
                                                  <li>
                                                       <Link href="/blog-grid">
                                                            <a>Brij Rani Kuteer, Vrindavan</a>
                                                       </Link>
                                                  </li>
                                                  <li>
                                                       <Link href="/blog-grid">
                                                            <a>Shri Radha Rani Township</a>
                                                       </Link>
                                                  </li>
                                                  <li>
                                                       <Link href="/blog-grid">
                                                            <a>Shri Radha Rani Township Phase 1</a>
                                                       </Link>
                                                  </li>
                                                  <li>
                                                       <Link href="/blog-grid">
                                                            <a>Shri Radha Rani Township Goverdhan</a>
                                                       </Link>
                                                  </li>
                                             </ul>
                                        </div>

                                        {/* Popular Post Widget */}
                                        {/* <div className="widget ltn__popular-post-widget go-top">
                                        <h4 className="ltn__widget-title ltn__widget-title-border-2">Leatest Blogs</h4>
                                        <ul>
                                             <li>
                                                  <div className="popular-post-widget-item clearfix">
                                                       <div className="popular-post-widget-img">
                                                            <Link href="/blog-details">
                                                                 <a>
                                                                      <img
                                                                           src={publicUrl + "assets/img/team/5.jpg"}
                                                                           alt="#"
                                                                      />
                                                                 </a>
                                                            </Link>
                                                       </div>
                                                       <div className="popular-post-widget-brief">
                                                            <h6>
                                                                 <Link href="/blog-details">
                                                                      Lorem ipsum dolor sit cing elit, sed do.
                                                                 </Link>
                                                            </h6>
                                                            <div className="ltn__blog-meta">
                                                                 <ul>
                                                                      <li className="ltn__blog-date">
                                                                           <a href="#">
                                                                                <i className="far fa-calendar-alt" />
                                                                                June 22, 2020
                                                                           </a>
                                                                      </li>
                                                                 </ul>
                                                            </div>
                                                       </div>
                                                  </div>
                                             </li>
                                        </ul>
                                   </div> */}
                                        {/* Social Media Widget */}
                                        <div className="widget ltn__social-media-widget">
                                             <h4 className="ltn__widget-title ltn__widget-title-border-2">Follow us</h4>
                                             <div className="ltn__social-media-2">
                                                  <ul>
                                                       <li>
                                                            <a href="#" title="Facebook">
                                                                 <i className="fab fa-facebook-f" />
                                                            </a>
                                                       </li>
                                                       <li>
                                                            <a href="#" title="Twitter">
                                                                 <i className="fab fa-twitter" />
                                                            </a>
                                                       </li>
                                                       <li>
                                                            <a href="#" title="Linkedin">
                                                                 <i className="fab fa-linkedin" />
                                                            </a>
                                                       </li>
                                                       <li>
                                                            <a href="#" title="Instagram">
                                                                 <i className="fab fa-instagram" />
                                                            </a>
                                                       </li>
                                                  </ul>
                                             </div>
                                        </div>

                                        {/* Banner Widget */}
                                        <div className="widget ltn__banner-widget d-none go-top">
                                             <Link href="/shop">
                                                  <a>
                                                       <img src={publicUrl + "assets/img/banner/2.jpg"} alt="#" />
                                                  </a>
                                             </Link>
                                        </div>
                                   </aside>
                              </div>
                         </div>
                    </div>
               </div>
          </>
     );
}

export default ShopDetails;
