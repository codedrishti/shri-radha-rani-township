import React, { useState, useEffect } from "react";
import Link from "next/link";
import parse from "html-react-parser";
const projectsListData = require("../../controller/ProjectList.json");

function ShopGridV1() {
     const [projList, setProjList] = useState([]);

     let publicUrl = "/";

     useEffect(() => {
          setProjList(projectsListData);
     }, []);

     return (
          <div>
               <div className="ltn__product-area ltn__product-gutter mb-100">
                    <div className="container">
                         <div className="row">
                              <div className="col-lg-12">
                                   <div className="ltn__shop-options">
                                        <ul>
                                             <li>
                                                  <div className="ltn__grid-list-tab-menu ">
                                                       <div className="nav">
                                                            <a
                                                                 className="active show"
                                                                 data-bs-toggle="tab"
                                                                 href="#liton_product_grid"
                                                            >
                                                                 <i className="fas fa-th-large" />
                                                            </a>
                                                            <a data-bs-toggle="tab" href="#liton_product_list">
                                                                 <i className="fas fa-list" />
                                                            </a>
                                                       </div>
                                                  </div>
                                             </li>
                                             <li>
                                                  <div className="short-by text-center">
                                                       <select className="nice-select">
                                                            <option>Default sorting</option>
                                                            <option>Sort by popularity</option>
                                                            <option>Sort by new arrivals</option>
                                                            <option>Sort by price: low to high</option>
                                                            <option>Sort by price: high to low</option>
                                                       </select>
                                                  </div>
                                             </li>
                                             <li>
                                                  <div className="showing-product-number text-right">
                                                       <span>Showing 9 of 20 results</span>
                                                  </div>
                                             </li>
                                        </ul>
                                   </div>
                                   <div className="tab-content ">
                                        <div className="tab-pane fade active show" id="liton_product_grid">
                                             <div className="ltn__product-tab-content-inner ltn__product-grid-view">
                                                  <div className="row">
                                                       <div className="col-lg-12">
                                                            {/* Search Widget */}
                                                            <div className="ltn__search-widget mb-30">
                                                                 <form action="#">
                                                                      <input
                                                                           type="text"
                                                                           name="search"
                                                                           placeholder="Search your keyword..."
                                                                      />
                                                                      <button type="submit">
                                                                           <i className="fas fa-search" />
                                                                      </button>
                                                                 </form>
                                                            </div>
                                                       </div>
                                                       {/* ltn__product-item */}
                                                       {projList.map((curElem, index) => (
                                                            <div className="col-lg-4 col-sm-6 col-12" key={index}>
                                                                 <div className="ltn__product-item ltn__product-item-4 ltn__product-item-5 text-center---">
                                                                      <div className="product-img">
                                                                           <a href={`/${curElem.permaLink}`}>
                                                                                <img
                                                                                     src={curElem.thumbnailImg}
                                                                                     alt="#"
                                                                                />
                                                                           </a>
                                                                           <div className="real-estate-agent">
                                                                                <div className="agent-img">
                                                                                     <Link href={curElem.authorLink}>
                                                                                          <img
                                                                                               src={curElem.authorImg}
                                                                                               alt="#"
                                                                                          />
                                                                                     </Link>
                                                                                </div>
                                                                           </div>
                                                                      </div>
                                                                      <div className="product-info">
                                                                           <div className="product-badge">
                                                                                <ul>
                                                                                     <li className="sale-badg">
                                                                                          {curElem.saleBadg}
                                                                                     </li>
                                                                                </ul>
                                                                           </div>
                                                                           <h2 className="product-title go-top">
                                                                                <a href={`/${curElem.permaLink}`}>
                                                                                     {curElem.projectName}
                                                                                </a>
                                                                           </h2>
                                                                           <div className="product-img-location">
                                                                                <ul>
                                                                                     <li className="go-top">
                                                                                          <Link href="/contact">
                                                                                               <a>
                                                                                                    <i className="flaticon-pin" />{" "}
                                                                                                    {curElem.location}
                                                                                               </a>
                                                                                          </Link>
                                                                                     </li>
                                                                                </ul>
                                                                           </div>
                                                                           <ul className="ltn__list-item-2--- ltn__list-item-2-before--- ltn__plot-brief">
                                                                                <li>
                                                                                     <span>{curElem.features.f1} </span>
                                                                                     ,
                                                                                </li>
                                                                                <li>
                                                                                     <span>{curElem.features.f2} </span>
                                                                                     Plots
                                                                                </li>
                                                                           </ul>
                                                                      </div>
                                                                      <div className="product-info-bottom">
                                                                           <div className="product-price">
                                                                                <span>
                                                                                     &#8377; {curElem.cost}
                                                                                     <label>/{curElem.area}</label>
                                                                                </span>
                                                                           </div>
                                                                      </div>
                                                                 </div>
                                                            </div>
                                                       ))}
                                                  </div>
                                             </div>
                                        </div>
                                        <div className="tab-pane fade" id="liton_product_list">
                                             <div className="ltn__product-tab-content-inner ltn__product-list-view">
                                                  <div className="row">
                                                       <div className="col-lg-12">
                                                            {/* Search Widget */}
                                                            <div className="ltn__search-widget mb-30">
                                                                 <form action="#">
                                                                      <input
                                                                           type="text"
                                                                           name="search"
                                                                           placeholder="Search your keyword..."
                                                                      />
                                                                      <button type="submit">
                                                                           <i className="fas fa-search" />
                                                                      </button>
                                                                 </form>
                                                            </div>
                                                       </div>
                                                       {/* ltn__product-item */}
                                                       {projList.map((curElem, index) => (
                                                            <div className="col-lg-12" key={index}>
                                                                 <div className="ltn__product-item ltn__product-item-4 ltn__product-item-5">
                                                                      <div className="product-img">
                                                                           <Link href={`/${curElem.permaLink}`}>
                                                                                <img
                                                                                     src={curElem.thumbnailImg}
                                                                                     alt="shri radha rani township"
                                                                                />
                                                                           </Link>
                                                                      </div>
                                                                      <div className="product-info">
                                                                           <div className="product-badge-price">
                                                                                <div className="product-badge">
                                                                                     <ul>
                                                                                          <li className="sale-badg">
                                                                                               {curElem.saleBadg}
                                                                                          </li>
                                                                                     </ul>
                                                                                </div>
                                                                                <div className="product-price">
                                                                                     <span>
                                                                                          &#8377; {curElem.cost}
                                                                                          <label>/{curElem.area}</label>
                                                                                     </span>
                                                                                </div>
                                                                           </div>
                                                                           <h2 className="product-title go-top">
                                                                                <Link href={`/${curElem.permaLink}`}>
                                                                                     {curElem.projectName}
                                                                                </Link>
                                                                           </h2>
                                                                           <div className="product-img-location">
                                                                                <ul>
                                                                                     <li className="go-top">
                                                                                          <Link href="/contact">
                                                                                               <a>
                                                                                                    <i className="flaticon-pin" />{" "}
                                                                                                    {curElem.location}
                                                                                               </a>
                                                                                          </Link>
                                                                                     </li>
                                                                                </ul>
                                                                           </div>
                                                                           <ul className="ltn__list-item-2--- ltn__list-item-2-before--- ltn__plot-brief">
                                                                                <li>
                                                                                     <span>Residential </span>
                                                                                     Plots
                                                                                </li>
                                                                                <li>
                                                                                     <span>Commercial </span>
                                                                                     Plots
                                                                                </li>
                                                                           </ul>
                                                                      </div>
                                                                      <div className="product-info-bottom">
                                                                           <div className="real-estate-agent">
                                                                                <div className="agent-img">
                                                                                     <Link href={curElem.authorLink}>
                                                                                          <img
                                                                                               src={curElem.authorImg}
                                                                                               alt="#"
                                                                                          />
                                                                                     </Link>
                                                                                </div>
                                                                                <div className="agent-brief go-top">
                                                                                     <h6>
                                                                                          <Link href="/team-details">
                                                                                               {curElem.authorName}
                                                                                          </Link>
                                                                                     </h6>
                                                                                     <small>
                                                                                          {curElem.designation}
                                                                                     </small>
                                                                                </div>
                                                                           </div>
                                                                      </div>
                                                                 </div>
                                                            </div>
                                                       ))}
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                                   <div className="ltn__pagination-area text-center">
                                        <div className="ltn__pagination">
                                             <ul>
                                                  <li>
                                                       <a href="#">
                                                            <i className="fas fa-angle-double-left" />
                                                       </a>
                                                  </li>
                                                  <li>
                                                       <a href="#">1</a>
                                                  </li>
                                                  <li className="active">
                                                       <a href="#">2</a>
                                                  </li>
                                                  <li>
                                                       <a href="#">3</a>
                                                  </li>
                                                  <li>
                                                       <a href="#">...</a>
                                                  </li>
                                                  <li>
                                                       <a href="#">10</a>
                                                  </li>
                                                  <li>
                                                       <a href="#">
                                                            <i className="fas fa-angle-double-right" />
                                                       </a>
                                                  </li>
                                             </ul>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     );
}

export default ShopGridV1;
