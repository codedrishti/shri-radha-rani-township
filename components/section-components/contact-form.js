import React, { useState } from "react";
import Link from "next/link";
import parse from "html-react-parser";
import $ from "jquery";
import Loader from "../loader";

function ContactForm() {
     let publicUrl = "/";

     const [form, setForm] = useState({
          name: "",
          phone: "",
          email: "",
          project: "",
          message: "",
     });
	const [isSubmit, setIsSubmit] = useState(false);

     const handleInputs = (e) => {
          const name = e.target.name;
          const value = e.target.value;

          setForm({
               ...form,
               [name]: value,
          });
     };

     const handleContact = async (e) => {
          e.preventDefault();

          const { name, phone, email, project, message } = form;

          setIsSubmit(true);

          try {
               const res = await fetch("/api/contact", {
                    method: "POST",
                    headers: {
                         Accept: "application/json, text/plain, */*",
                         "Content-Type": "application/json",
                    },
                    body: JSON.stringify(form),
               });

               const result = await res.json();

               if (res.status === 201) {
                    window.alert("Data submitted successfully!");
                    setForm({});
                    setIsSubmit(false);
               }
          } catch (err) {
               console.log(err);
          }
     };

     return (
          <>
               {isSubmit ? (
                    <Loader />
               ) : (
                    <div className="ltn__contact-message-area mb-120 mb--100">
                         <div className="container">
                              <div className="row">
                                   <div className="col-lg-12">
                                        <div className="ltn__form-box contact-form-box box-shadow white-bg">
                                             <h4 className="title-2">Get A Quote</h4>
                                             <form id="contact-form" onSubmit={handleContact}>
                                                  <div className="row">
                                                       <div className="col-md-6">
                                                            <div className="input-item input-item-name ltn__custom-icon">
                                                                 <input
                                                                      type="text"
                                                                      name="name"
                                                                      placeholder="Enter your name"
                                                                      value={form.name}
                                                                      onChange={handleInputs}
                                                                 />
                                                            </div>
                                                       </div>
                                                       <div className="col-md-6">
                                                            <div className="input-item input-item-email ltn__custom-icon">
                                                                 <input
                                                                      type="email"
                                                                      name="email"
                                                                      placeholder="Enter email address"
                                                                      value={form.email}
                                                                      onChange={handleInputs}
                                                                 />
                                                            </div>
                                                       </div>
                                                       <div className="col-md-6">
                                                            <div className="input-item input-item-phone ltn__custom-icon">
                                                                 <input
                                                                      type="text"
                                                                      name="phone"
                                                                      placeholder="Enter phone number"
                                                                      value={form.phone}
                                                                      onChange={handleInputs}
                                                                 />
                                                            </div>
                                                       </div>
                                                       <div className="col-md-6">
                                                            <div className="input-item">
                                                                 <select
                                                                      className="nice-select"
                                                                      name="project"
                                                                      onChange={handleInputs}
                                                                 >
                                                                      <option value="">Select Property</option>
                                                                      <option value="SRRT">
                                                                           Shri Radha Rani Township{" "}
                                                                      </option>
                                                                      <option value="SRRT1">
                                                                           Shri Radha Rani Township Phase 1{" "}
                                                                      </option>
                                                                      <option value="BRKT">
                                                                           Brij Rani Kuteer Township
                                                                      </option>
                                                                      <option value="SRRTG">
                                                                           Shri Radha Rani Township Govardhab
                                                                      </option>
                                                                 </select>
                                                            </div>
                                                       </div>
                                                  </div>
                                                  <div className="input-item input-item-textarea ltn__custom-icon">
                                                       <textarea
                                                            name="message"
                                                            placeholder="Enter message"
                                                            defaultValue={""}
                                                            value={form.message}
                                                            onChange={handleInputs}
                                                       />
                                                  </div>
                                                  <p>
                                                       <label className="input-info-save mb-0">
                                                            <input type="checkbox" name="agree" checked={true} /> Save
                                                            information to send me the project details & for the future.
                                                       </label>
                                                  </p>
                                                  <div className="btn-wrapper mt-0">
                                                       <button
                                                            className="btn theme-btn-1 btn-effect-1 text-uppercase"
                                                            type="submit"
                                                       >
                                                            submit
                                                       </button>
                                                  </div>
                                                  <p className="form-messege mb-0 mt-20" />
                                             </form>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               )}
          </>
     );
}

export default ContactForm;
