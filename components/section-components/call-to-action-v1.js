import React, { Component } from "react";
import Link from "next/link";
import parse from "html-react-parser";

class CallToActonV1 extends Component {
  render() {
    let publicUrl = "/";
    let imagealt = "image";

    return (
      <div className="ltn__call-to-action-area call-to-action-6 before-bg-bottom">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="call-to-action-inner call-to-action-inner-6 ltn__secondary-bg position-relative text-center---">
                <div className="coll-to-info text-color-white">
                  <h1>Are you looking for earning source?</h1>
                  <p>Become a Shri Radha Rani Township Associate Today! Its a way to achive the dreams & financial free life.</p>
                </div>
                <div className="btn-wrapper go-top">
                  <Link href="/contact">
                    <a className="btn btn-effect-3 btn-white" data-bs-toggle="modal" data-bs-target="#associate_Enquiry_Form">
                      Associate with us <i className="icon-next" />
                    </a>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CallToActonV1;
