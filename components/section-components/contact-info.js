import React, { Component } from 'react';
import Link from 'next/link';
import parse from 'html-react-parser';

class ContactInfo extends Component {

    render() {

        let publicUrl = '/'

    return <div className="ltn__contact-address-area mb-90">
				<div className="container">
				<div className="row">
					<div className="col-lg-4">
					<div className="ltn__contact-address-item ltn__contact-address-item-3 box-shadow">
						<div className="ltn__contact-address-icon">
						<img src={publicUrl+"assets/img/icons/10.png"} alt="Icon Image" />
						</div>
						<h3>Email Address</h3>
						<p>info@shriradharanitownship.com <br />
						srrtownship@gmail.com</p>
					</div>
					</div>
					<div className="col-lg-4">
					<div className="ltn__contact-address-item ltn__contact-address-item-3 box-shadow">
						<div className="ltn__contact-address-icon">
						<img src={publicUrl+"assets/img/icons/11.png"} alt="Icon Image" />
						</div>
						<h3>Phone Number</h3>
						<p>07550400495 <br /> 09873632575</p>
					</div>
					</div>
					<div className="col-lg-4">
					<div className="ltn__contact-address-item ltn__contact-address-item-3 box-shadow">
						<div className="ltn__contact-address-icon">
						<img src={publicUrl+"assets/img/icons/12.png"} alt="Icon Image" />
						</div>
						<h3>Head Office</h3>
						<p>15/1, Main Mathura Road <br />
						Faridabad, India - 121003</p>
					</div>
					</div>
				</div>
				</div>
			</div>
        }
}

export default ContactInfo