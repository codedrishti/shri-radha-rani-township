import React, { Component } from "react";
import Link from "next/link";
import parse from "html-react-parser";

class ProductSliderV2 extends Component {
  render() {
    let publicUrl = "/";

    return (
      <div
        className="ltn__search-by-place-area before-bg-top bg-image-top--- pt-115 pb-70"
        data-bs-bg={publicUrl + "assets/img/bg/20.jpg"}
      >
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="section-title-area ltn__section-title-2--- text-center---">
                <h6 className="section-subtitle section-subtitle-2--- ltn__secondary-color">
                  Area Properties
                </h6>
                <h1 className="section-title">
                  Find Your Dream Property <br />
                  Search By Area
                </h1>
              </div>
            </div>
          </div>
          <div className="row ltn__search-by-place-slider-1-active slick-arrow-1 go-top">
            <div className="col-lg-4">
              <div className="ltn__search-by-place-item">
                <div className="search-by-place-img">
                  <Link href="/product-details">
                    <a>
                      <img
                        src="https://res.cloudinary.com/dpfketvjx/image/upload/v1647848421/Projects/thumbnail/brij-rani-kuteer_dsx2uz.jpg"
                        alt="#"
                      />
                    </a>
                  </Link>
                  <div className="search-by-place-badge">
                    <ul>
                      <li>2 Properties</li>
                    </ul>
                  </div>
                </div>
                <div className="search-by-place-info">
                  <h6>
                    <Link href="/contact">Vrindavan, Mathura</Link>
                  </h6>
                  <h4>
                    <Link href="/product-details">Brij Rani Kuteer Township Vrindavan</Link>
                  </h4>
                  <div className="search-by-place-btn">
                    <Link href="/product-details">
                      <a>
                        View Property <i className="flaticon-right-arrow" />
                      </a>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="ltn__search-by-place-item">
                <div className="search-by-place-img">
                  <Link href="/product-details">
                    <a>
                      <img
                        src="https://res.cloudinary.com/dpfketvjx/image/upload/v1647848420/Projects/thumbnail/shri-radha-rani-township_tdtpaw.jpg"
                        alt="#"
                      />
                    </a>
                  </Link>
                  <div className="search-by-place-badge">
                    <ul>
                      <li>5 Properties</li>
                    </ul>
                  </div>
                </div>
                <div className="search-by-place-info">
                  <h6>
                    <Link href="/contact">Barsana(sanket), Mathura</Link>
                  </h6>
                  <h4>
                    <Link href="/product-details">Shri Radha Rani Township Sanket</Link>
                  </h4>
                  <div className="search-by-place-btn">
                    <Link href="/product-details">
                      <a>
                        {" "}
                        View Property <i className="flaticon-right-arrow" />
                      </a>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="ltn__search-by-place-item">
                <div className="search-by-place-img">
                  <Link href="/product-details">
                    <a>
                      <img
                        src="https://res.cloudinary.com/dpfketvjx/image/upload/v1647848421/Projects/thumbnail/shri-radha-rani-township-phase-1_bwivfk.jpg"
                        alt="#"
                      />
                    </a>
                  </Link>
                  <div className="search-by-place-badge">
                    <ul>
                      <li>9 Properties</li>
                    </ul>
                  </div>
                </div>
                <div className="search-by-place-info">
                  <h6>
                    <Link href="/contact">Barsana(ajnokh), Mathura</Link>
                  </h6>
                  <h4>
                    <a href="shri-radha-rani-township-phase-1">Shri Radha Rani Township Phase I Ajnokh</a>
                  </h4>
                  <div className="search-by-place-btn">
                    <Link href="/product-details">
                      <a>
                        View Property <i className="flaticon-right-arrow" />
                      </a>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="ltn__search-by-place-item">
                <div className="search-by-place-img">
                  <Link href="/product-details">
                    <a>
                      <img
                        src="https://res.cloudinary.com/dpfketvjx/image/upload/v1647848421/Projects/thumbnail/shri-radha-rani-township-govardhan_ecbjj4.jpg"
                        alt="#"
                      />
                    </a>
                  </Link>
                  <div className="search-by-place-badge">
                    <ul>
                      <li>5 Properties</li>
                    </ul>
                  </div>
                </div>
                <div className="search-by-place-info">
                  <h6>
                    <Link href="/contact">Govardhan, Mathura</Link>
                  </h6>
                  <h4>
                    <a href="shri-radha-rani-township-govardhan">Shri Radha Rani Township Govardhan</a>
                  </h4>
                  <div className="search-by-place-btn">
                    <Link href="/product-details">
                      <a>
                        View Property <i className="flaticon-right-arrow" />
                      </a>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
            {/*  */}
            <div className="col-lg-4">
              <div className="ltn__search-by-place-item">
                <div className="search-by-place-img">
                  <Link href="/product-details">
                    <a>
                      <img
                        src="https://res.cloudinary.com/dpfketvjx/image/upload/v1648795126/Projects/thumbnail/maa-kaila-devi-township-001_onhcs5.jpg"
                        alt="#"
                      />
                    </a>
                  </Link>
                  <div className="search-by-place-badge">
                    <ul>
                      <li>5 Properties</li>
                    </ul>
                  </div>
                </div>
                <div className="search-by-place-info">
                  <h6>
                    <Link href="/contact">Agra</Link>
                  </h6>
                  <h4>
                    <a href="shri-radha-rani-township-govardhan">Maa Kaila Devi Township Agra</a>
                  </h4>
                  <div className="search-by-place-btn">
                    <Link href="/product-details">
                      <a>
                        View Property <i className="flaticon-right-arrow" />
                      </a>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ProductSliderV2;
