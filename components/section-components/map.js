import React, { Component } from 'react';
import Link from 'next/link';
import parse from 'html-react-parser';

class Map extends Component {

    render() {

        let publicUrl = '/'

        return <div className="google-map mb-120">
            
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.9666495549304!2d77.38493071434301!3d27.687425632975543!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3973178faacd9f11%3A0x233f7f789e5da295!2sKRS%20Group%20Shri%20Radha%20Rani%20Township%20Barsana!5e0!3m2!1sen!2sin!4v1648487861331!5m2!1sen!2sin" width="100%" height="100%" frameBorder={0} allowFullScreen aria-hidden="false" tabIndex={0} />
        </div>
    }
}

export default Map