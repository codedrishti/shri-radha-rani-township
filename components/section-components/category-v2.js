import React, { Component } from "react";
import Link from "next/link";
import parse from "html-react-parser";

class CategoryV2 extends Component {
  render() {
    let publicUrl = "/";
    let imagealt = "image";

    return (
      <div className="ltn__category-area ltn__product-gutter section-bg-1--- pt-115 pb-70">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="section-title-area ltn__section-title-2--- text-center">
                <h6 className="section-subtitle section-subtitle-2--- ltn__secondary-color">
                  Our Aminities
                </h6>
                <h1 className="section-title">Township Aminities & Facilities</h1>
              </div>
            </div>
          </div>
          <div className="row ltn__category-slider-active--- slick-arrow-1 justify-content-center go-top">
            <div className="col-lg-3 col-md-4 col-sm-6 col-6">
              <div className="ltn__category-item ltn__category-item-5 ltn__category-item-5-2 text-center---">
                <Link href="/shop">
                  <a>
                    <span className="category-icon">
                      <i className="flaticon-park" />
                    </span>
                    <span className="category-number">01</span>
                    <span className="category-title">Parks</span>
                    <span className="category-brief">
                    Township has many parks for residents to enjoy their healthy lifestyle.
                    </span>
                    <span className="category-btn d-none">
                      <i className="flaticon-right-arrow" />
                    </span>
                  </a>
                </Link>
              </div>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-6 col-6">
              <div className="ltn__category-item ltn__category-item-5 ltn__category-item-5-2 text-center---">
                <Link href="/shop">
                  <a>
                    <span className="category-icon">
                      <i className="flaticon-swimming" />
                    </span>
                    <span className="category-number">02</span>
                    <span className="category-title">Swimming Pool</span>
                    <span className="category-brief">
                    A place with a swimming pool is an area that children will love to play.
                    </span>
                    <span className="category-btn d-none">
                      <i className="flaticon-right-arrow" />
                    </span>
                  </a>
                </Link>
              </div>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-6 col-6">
              <div className="ltn__category-item ltn__category-item-5 ltn__category-item-5-2 text-center---">
                <Link href="/shop">
                  <a>
                    <span className="category-icon">
                      <i className="flaticon-secure-shield" />
                    </span>
                    <span className="category-number">03</span>
                    <span className="category-title">Private Security</span>
                    <span className="category-brief">
                      Security guards, CCTV cameras and barbed wire on the boundary of township.
                    </span>
                    <span className="category-btn d-none">
                      <i className="flaticon-right-arrow" />
                    </span>
                  </a>
                </Link>
              </div>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-6 col-6">
              <div className="ltn__category-item ltn__category-item-5 ltn__category-item-5-2 text-center---">
                <Link href="/shop">
                  <a>
                    <span className="category-icon">
                      <i className="flaticon-stethoscope" />
                    </span>
                    <span className="category-number">04</span>
                    <span className="category-title">Nursing Home</span>
                    <span className="category-brief">
                    Medical facilities are a must for the wellbeing of your family.
                    </span>
                    <span className="category-btn d-none">
                      <i className="flaticon-right-arrow" />
                    </span>
                  </a>
                </Link>
              </div>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-6 col-6">
              <div className="ltn__category-item ltn__category-item-5 ltn__category-item-5-2 text-center---">
                <Link href="/shop">
                  <a>
                    <span className="category-icon">
                      <i className="flaticon-book" />
                    </span>
                    <span className="category-number">05</span>
                    <span className="category-title">School Area</span>
                    <span className="category-brief">
                    Kids are a huge part of the society and they should be educated too. 
                    </span>
                    <span className="category-btn d-none">
                      <i className="flaticon-right-arrow" />
                    </span>
                  </a>
                </Link>
              </div>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-6 col-6">
              <div className="ltn__category-item ltn__category-item-5 ltn__category-item-5-2 text-center---">
                <Link href="/shop">
                  <a>
                    <span className="category-icon">
                      <i className="fa fa-trash" />
                    </span>
                    <span className="category-number">06</span>
                    <span className="category-title">STP</span>
                    <span className="category-brief">
                      We are developing the STP plant in townships to manage the polluted water.
                    </span>
                    <span className="category-btn d-none">
                      <i className="flaticon-right-arrow" />
                    </span>
                  </a>
                </Link>
              </div>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-6 col-6">
              <div className="ltn__category-item ltn__category-item-5 ltn__category-item-5-2 text-center---">
                <Link href="/shop">
                  <a>
                    <span className="category-icon">
                      <i className="fa fa-store-alt" />
                    </span>
                    <span className="category-number">07</span>
                    <span className="category-title">Market</span>
                    <span className="category-brief">
                      In the township, we have commercial spaces for market and shops.
                    </span>
                    <span className="category-btn d-none">
                      <i className="flaticon-right-arrow" />
                    </span>
                  </a>
                </Link>
              </div>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-6 col-6">
              <div className="ltn__category-item ltn__category-item-5 ltn__category-item-5-2 text-center---">
                <Link href="/shop">
                  <a>
                    <span className="category-icon">
                      <i className="fa fa-cloud-rain" />
                    </span>
                    <span className="category-number">08</span>
                    <span className="category-title">RWH</span>
                    <span className="category-brief">
                      To manage the rain water we have developed the rain water harvesting system.
                    </span>
                    <span className="category-btn d-none">
                      <i className="flaticon-right-arrow" />
                    </span>
                  </a>
                </Link>
              </div>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-6 col-6">
              <div className="ltn__category-item ltn__category-item-5 ltn__category-item-5-2 text-center---">
                <Link href="/shop">
                  <a>
                    <span className="category-icon">
                      <i className="fa fa-road" />
                    </span>
                    <span className="category-number">09</span>
                    <span className="category-title">Paved Roads</span>
                    <span className="category-brief">
                    Wide and paved roads are in the township and are a necessity for all.
                    </span>
                    <span className="category-btn d-none">
                      <i className="flaticon-right-arrow" />
                    </span>
                  </a>
                </Link>
              </div>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-6 col-6">
              <div className="ltn__category-item ltn__category-item-5 ltn__category-item-5-2 text-center---">
                <Link href="/shop">
                  <a>
                    <span className="category-icon">
                      <i className="fa fa-lightbulb" />
                    </span>
                    <span className="category-number">10</span>
                    <span className="category-title">Electricity</span>
                    <span className="category-brief">
                    We have made electricity available in our township for everyone.
                    </span>
                    <span className="category-btn d-none">
                      <i className="flaticon-right-arrow" />
                    </span>
                  </a>
                </Link>
              </div>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-6 col-6">
              <div className="ltn__category-item ltn__category-item-5 ltn__category-item-5-2 text-center---">
                <Link href="/shop">
                  <a>
                    <span className="category-icon">
                      <i className="fa fa-om" />
                    </span>
                    <span className="category-number">11</span>
                    <span className="category-title">Temple</span>
                    <span className="category-brief">
                      The temple is a great place to learn meditation and it gives peace.
                    </span>
                    <span className="category-btn d-none">
                      <i className="flaticon-right-arrow" />
                    </span>
                  </a>
                </Link>
              </div>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-6 col-6">
              <div className="ltn__category-item ltn__category-item-5 ltn__category-item-5-2 text-center---">
                <Link href="/shop">
                  <a>
                    <span className="category-icon">
                    <i className="fa fa-water"/>
                    </span>
                    <span className="category-number">12</span>
                    <span className="category-title">24x7 Water Supply</span>
                    <span className="category-brief">
                      Water is necessity of living life. And it is available 24/7 in every project by OHT.
                    </span>
                    <span className="category-btn d-none">
                      <i className="flaticon-right-arrow" />
                    </span>
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CategoryV2;
