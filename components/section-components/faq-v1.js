import React, { Component } from "react";
import Link from "next/link";
import parse from "html-react-parser";

class FaqV1 extends Component {
     render() {
          let publicUrl = "/";

          return (
               <div className="ltn__faq-area mb-100">
                    <div className="container">
                         <div className="row">
                              <p>
                                   You can find this information will familiarize yourself with, laws & rules and other
                                   information relating to the property.
                              </p>

                              <div className="col-lg-8">
                                   <div className="mb-5">
                                        <h4>Note:</h4>
                                        <ul className="ltn__list-item-half ltn__list-item-half-2 list-item-margin clearfix">
                                             <li style={{ width: "100%" }}>
                                                  <i className="icon-done"></i>Free Site Visit Available For Our All
                                                  Projects
                                             </li>
                                             <li style={{ width: "100%" }}>
                                                  <i className="icon-done"></i>0% Interest Rate on EMI
                                             </li>
                                             <li style={{ width: "100%" }}>
                                                  <i className="icon-done"></i>EMI Facilities Available For 15 Month to
                                                  40 Month
                                             </li>
                                        </ul>
                                   </div>
                                   <div className="ltn__faq-inner ltn__faq-inner-2">
                                        <div id="accordion_2">
                                             {/* card */}
                                             <div className="card">
                                                  <h6
                                                       className="collapsed ltn__card-title"
                                                       data-bs-toggle="collapse"
                                                       data-bs-target="#faq-item-2-0"
                                                       aria-expanded="false"
                                                  >
                                                       Shri Radha Rani Township Contact Information?
                                                  </h6>
                                                  <div
                                                       id="faq-item-2-0"
                                                       className="collapse show"
                                                       data-bs-parent="#accordion_2"
                                                  >
                                                       <div className="card-body">
                                                            <p>
                                                                 {" "}
                                                                 You can contact on phone: 07550-400-495, email:
                                                                 info@shriradharanitownship.com or srrtownship@gmail.com
                                                            </p>
                                                       </div>
                                                  </div>
                                             </div>
                                             {/* card */}
                                             <div className="card">
                                                  <h6
                                                       className="collapsed ltn__card-title"
                                                       data-bs-toggle="collapse"
                                                       data-bs-target="#faq-item-2-1"
                                                       aria-expanded="false"
                                                  >
                                                       How can I visit the project before plot booking?
                                                  </h6>
                                                  <div
                                                       id="faq-item-2-1"
                                                       className="collapse"
                                                       data-bs-parent="#accordion_2"
                                                  >
                                                       <div className="card-body">
                                                            <p>
                                                                 {" "}
                                                                 If you are looking to visit our project, please contact
                                                                 us on above given details and we will schedule your
                                                                 visit. Our project is located at diffrent places, so we
                                                                 will schedule you visit as per your interest.{" "}
                                                            </p>
                                                       </div>
                                                  </div>
                                             </div>
                                             {/* card */}
                                             <div className="card">
                                                  <h6
                                                       className="ltn__card-title"
                                                       data-bs-toggle="collapse"
                                                       data-bs-target="#faq-item-2-2"
                                                       aria-expanded="true"
                                                  >
                                                       Why is it considered necessary to register Agreement for Sale?
                                                       What is the purpose of registration?
                                                  </h6>
                                                  <div
                                                       id="faq-item-2-2"
                                                       className="collapse"
                                                       data-bs-parent="#accordion_2"
                                                  >
                                                       <div className="card-body">
                                                            <p>
                                                                 The Registration Act, 1908, the Transfer of Property
                                                                 Act, 1882 and the Real Estate (Regulation and
                                                                 Development) Act, 2016 mandates the registration of an
                                                                 agreement for sale of an immovable property. By
                                                                 registering the agreement for sale of an immovable
                                                                 property, it becomes a permanent public record.
                                                                 Further, a person is considered as the legal owner of
                                                                 an immovable property only after he gets such property
                                                                 registered in his name.
                                                            </p>
                                                       </div>
                                                  </div>
                                             </div>
                                             {/* card */}
                                             <div className="card">
                                                  <h6
                                                       className="collapsed ltn__card-title"
                                                       data-bs-toggle="collapse"
                                                       data-bs-target="#faq-item-2-3"
                                                       aria-expanded="false"
                                                  >
                                                       Where is the projects located?
                                                  </h6>
                                                  <div
                                                       id="faq-item-2-3"
                                                       className="collapse"
                                                       data-bs-parent="#accordion_2"
                                                  >
                                                       <div className="card-body">
                                                            <p>
                                                                 {" "}
                                                                 Projects are located in Barsana (Sanket and Ajnokh),
                                                                 Goverdhan(near chhatigrah), Vrindavan and Agra. Contact
                                                                 us for more details.{" "}
                                                            </p>
                                                       </div>
                                                  </div>
                                             </div>
                                             {/* card */}
                                             <div className="card">
                                                  <h6
                                                       className="collapsed ltn__card-title"
                                                       data-bs-toggle="collapse"
                                                       data-bs-target="#faq-item-2-4"
                                                       aria-expanded="false"
                                                  >
                                                       Is project approved for Residential Development?
                                                  </h6>
                                                  <div
                                                       id="faq-item-2-4"
                                                       className="collapse"
                                                       data-bs-parent="#accordion_2"
                                                  >
                                                       <div className="card-body">
                                                            <p>
                                                                 Our projects are athorised for residential development
                                                                 and 143 approved by the goverment bodies which means
                                                                 Change of Land Use. We can build the home, shop or
                                                                 other constructions without hesitation.
                                                            </p>
                                                       </div>
                                                  </div>
                                             </div>
                                             {/* card */}
                                             <div className="card">
                                                  <h6
                                                       className="collapsed ltn__card-title"
                                                       data-bs-toggle="collapse"
                                                       data-bs-target="#faq-item-2-5"
                                                       aria-expanded="false"
                                                  >
                                                       Is there commercial and residential both plots are available?
                                                  </h6>
                                                  <div
                                                       id="faq-item-2-5"
                                                       className="collapse"
                                                       data-bs-parent="#accordion_2"
                                                  >
                                                       <div className="card-body">
                                                            <p>
                                                                 In the every township there is set of rules provided by
                                                                 the goverment and by these rules we have to develop the
                                                                 commecial land also. So commercial plots are also
                                                                 available along with residential plots.
                                                            </p>
                                                       </div>
                                                  </div>
                                             </div>
                                             {/* card */}
                                             <div className="card">
                                                  <h6
                                                       className="collapsed ltn__card-title"
                                                       data-bs-toggle="collapse"
                                                       data-bs-target="#faq-item-2-6"
                                                       aria-expanded="false"
                                                  >
                                                       Is there regular supply of water and electricity?
                                                  </h6>
                                                  <div
                                                       id="faq-item-2-6"
                                                       className="collapse"
                                                       data-bs-parent="#accordion_2"
                                                  >
                                                       <div className="card-body">
                                                            <p>
                                                                 Yes, Regulat supply of water by the OHT. This is
                                                                 develop in the township for 24/7 water supply. And the
                                                                 electricity is supplied by the local govt. authorities
                                                                 or private electricity company.
                                                            </p>
                                                       </div>
                                                  </div>
                                             </div>
                                             {/* card */}
                                             <div className="card">
                                                  <h6
                                                       className="collapsed ltn__card-title"
                                                       data-bs-toggle="collapse"
                                                       data-bs-target="#faq-item-2-7"
                                                       aria-expanded="false"
                                                  >
                                                       How do I make payment of my plot installment?
                                                  </h6>
                                                  <div
                                                       id="faq-item-2-7"
                                                       className="collapse"
                                                       data-bs-parent="#accordion_2"
                                                  >
                                                       <div className="card-body">
                                                            <p>
                                                                 We are modern time company so that we have various
                                                                 options to pay your EMIs. You can pay by NEFT, RTGS,
                                                                 IMGS, ECS, Debit, Credit or UPIs also. If you wants to
                                                                 pay by cheque so you can also use this payment method.
                                                            </p>
                                                       </div>
                                                  </div>
                                             </div>
                                             {/* card */}
                                             <div className="card">
                                                  <h6
                                                       className="collapsed ltn__card-title"
                                                       data-bs-toggle="collapse"
                                                       data-bs-target="#faq-item-2-8"
                                                       aria-expanded="false"
                                                  >
                                                       How I make sure the property security?
                                                  </h6>
                                                  <div
                                                       id="faq-item-2-8"
                                                       className="collapse"
                                                       data-bs-parent="#accordion_2"
                                                  >
                                                       <div className="card-body">
                                                            <p>
                                                                 The security in the township is make sure by the
                                                                 company and RWA so you don&#39;t need to worry about
                                                                 your security. Guards are always available on the
                                                                 township gate. Every movement of inn and out are
                                                                 monitored by CCTV also. The whole project are coved by
                                                                 barbed wire on the boundary of township wall.
                                                            </p>
                                                       </div>
                                                  </div>
                                             </div>
                                             <div className="card">
                                                  <h6
                                                       className="collapsed ltn__card-title"
                                                       data-bs-toggle="collapse"
                                                       data-bs-target="#faq-item-2-9"
                                                       aria-expanded="false"
                                                  >
                                                       Registry are available on 50% Payment?
                                                  </h6>
                                                  <div
                                                       id="faq-item-2-9"
                                                       className="collapse"
                                                       data-bs-parent="#accordion_2"
                                                  >
                                                       <div className="card-body">
                                                            <p>
                                                                 Yes, you can do the registry of your plot on 50%
                                                                 payment only. And you can get possession of the plot
                                                                 also.
                                                            </p>
                                                       </div>
                                                  </div>
                                             </div>
                                        </div>
                                        <div className="need-support text-center mt-100">
                                             <h2>Still need help? Reach out to support 24/7:</h2>
                                             <div className="btn-wrapper mb-30 go-top">
                                                  <a href="contact" className="theme-btn-1 btn">
                                                       Contact Us
                                                  </a>
                                             </div>
                                             <h3>
                                                  <a href="tel:07550400495">
                                                       <i className="fas fa-phone" /> 075-5040-0495
                                                  </a>
                                             </h3>
                                        </div>
                                   </div>
                              </div>
                              <div className="col-lg-4">
                                   <aside className="sidebar-area ltn__right-sidebar">
                                        {/* Newsletter Widget */}
                                        {/* <div className="widget ltn__search-widget ltn__newsletter-widget">
                                             <h6 className="ltn__widget-sub-title">// subscribe</h6>
                                             <h4 className="ltn__widget-title">Get Newsletter</h4>
                                             <form action="#">
                                                  <input type="text" name="search" placeholder="Search" />
                                                  <button type="submit">
                                                       <i className="fas fa-search" />
                                                  </button>
                                             </form>
                                             <div className="ltn__newsletter-bg-icon">
                                                  <i className="fas fa-envelope-open-text" />
                                             </div>
                                        </div> */}
                                        {/* Banner Widget */}
                                        <div className="widget ltn__banner-widget go-top">
                                             <a role="button" data-bs-toggle="modal" data-bs-target="#enquiryPopup">
                                                  <img
                                                       src={publicUrl + "assets/img/banner/banner-3.jpg"}
                                                       alt="Banner Image"
                                                  />
                                             </a>
                                        </div>
                                        <div className="widget ltn__banner-widget go-top">
                                             <a
                                                  role="button"
                                                  data-bs-toggle="modal"
                                                  data-bs-target="#associate_Enquiry_Form"
                                             >
                                                  <img
                                                       src={publicUrl + "assets/img/banner/banner-3-1.jpg"}
                                                       alt="Banner Image"
                                                  />
                                             </a>
                                        </div>
                                   </aside>
                              </div>
                         </div>
                    </div>
               </div>
          );
     }
}

export default FaqV1;
