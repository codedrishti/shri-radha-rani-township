import React, { useState, useRef } from "react";
import Link from "next/link";
import parse from "html-react-parser";
import Loader from "../loader";

function CallToActonV4() {
     const ref = useRef(null);

     let publicUrl = "/";

     const [ppForm, setPPForm] = useState({
          subject: "Download Broucher - Enquiry",
          name: "",
          email: "",
          phone: "",
     });
     const [isSubmit, setIsSubmit] = useState(false);

     const handleInputs = (e) => {
          const name = e.target.name;
          const value = e.target.value;

          setPPForm({
               ...ppForm,
               [name]: value,
          });
     };

     const handleDownload = async (e) => {
          e.preventDefault();

          const { subject, name, email, phone } = ppForm;

          setIsSubmit(true);

          try {
               const res = await fetch("/api/enquiry/projapi", {
                    method: "POST",
                    headers: {
                         Accept: "application/json, text/plain, */*",
                         "Content-Type": "application/json",
                    },
                    body: JSON.stringify(ppForm),
               });

               const result = await res.json();

               if (res.status === 201) {
                    window.alert("Data submitted successfully!");
                    setPPForm({});
                    setIsSubmit(false);
                    ref.current.click();
                    window.open("https://drive.google.com/file/d/1hP60lr8gKb5jJ7FsFAWzAZRnmaUL-lJc/view?usp=sharing","_blank");
               }
          } catch (err) {
               console.log(err);
          }
     };

     return (
          <>
               <div
                    className="call-to-action-area call-to-action-5 bg-image bg-overlay-theme-90 pt-40 pb-25"
                    data-bs-bg={publicUrl + "assets/img/bg/13.jpg"}
               >
                    <div className="container">
                         <div className="row">
                              <div className="col-lg-7">
                                   <div className="call-to-action-inner call-to-action-inner-5 text-decoration text-center go-top">
                                        <h2 className="white-color">
                                             24/7 Availability, Make <Link href="/contact">An Appointment</Link>
                                        </h2>
                                   </div>
                              </div>
                              <div className="col-lg-5">
                                   <div className="btn-wrapper go-top">
                                        <a
                                             className="btn btn-effect-3 btn-white"
                                             data-bs-toggle="modal"
                                             data-bs-target="#download_brochure_modal"
                                        >
                                             Download Brochure <i className="icon-next" />
                                        </a>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
               {/* Download Brochure Modal */}
               <div className="modal fade" id="download_brochure_modal" tabIndex={-1} role="dialog">
                    <div className="modal-dialog modal-md" role="document">
                         <div className="modal-content">
                              <div className="modal-header">
                                   <h5 className="modal-title">BASIC INFORMATION</h5>
                                   <button type="button" className="close" data-bs-dismiss="modal" aria-label="Close" ref={ref}>
                                        <span aria-hidden="true">×</span>
                                   </button>
                              </div>
                              {isSubmit ? (
                                   <Loader />
                              ) : (
                                   <form onSubmit={handleDownload}> 
                                        <div className="modal-body">
                                             <div className="form-group">
                                                  <input
                                                       type="text"
                                                       className="form-control"
                                                       name="name"
                                                       placeholder="Name"
                                                       value={ppForm.name}
                                                       onChange={handleInputs}
                                                       required
                                                  />
                                             </div>
                                             <div className="form-group">
                                                  <input
                                                       type="tel"
                                                       className="form-control"
                                                       name="phone"
                                                       placeholder="Phone"
                                                       value={ppForm.phone}
                                                       onChange={handleInputs}
                                                       required
                                                  />
                                             </div>
                                             <div className="form-group">
                                                  <input
                                                       type="email"
                                                       className="form-control"
                                                       name="email"
                                                       placeholder="Email"
                                                       value={ppForm.email}
                                                       onChange={handleInputs}
                                                       required
                                                  />
                                             </div>
                                             <div className="form-group">
                                                  <div className="form-check">
                                                       <input
                                                            className="form-check-input"
                                                            type="checkbox"
                                                            id="ppIvisit"
                                                            checked={true}
                                                       />
                                                       <label className="form-check-label" htmlFor="gridCheck">
                                                            I am interested in site visits.
                                                       </label>
                                                  </div>
                                             </div>
                                             <div className="form-group">
                                                  <div className="form-check">
                                                       <input
                                                            className="form-check-input"
                                                            type="checkbox"
                                                            id="ppTnC"
                                                            checked={true}
                                                       />
                                                       <label className="form-check-label" htmlFor="gridCheck">
                                                            I agree to be contacted by shriradhanitownship.com for
                                                            properties or related services via WhatsApp, phone, sms,
                                                            e-mail etc.
                                                       </label>
                                                  </div>
                                             </div>
                                        </div>
                                        <div className="modal-footer">
                                             <button type="submit" className="theme-btn-1 btn btn-effect-2 btn-sm">
                                                  <i className="fa fa-download"></i>&nbsp; Download Brochure
                                             </button>
                                        </div>
                                   </form>
                              )}
                         </div>
                    </div>
               </div>
          </>
     );
}

export default CallToActonV4;
