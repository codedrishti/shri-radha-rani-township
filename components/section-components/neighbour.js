import React, { Component } from 'react';
import Link from 'next/link';
import parse from 'html-react-parser';

class Neighbour extends Component {

    render() {

        let publicUrl = '/'
        let imagealt = 'image'

    return <div className="neighbour-area section-bg-1 pt-118 pb-120">
			  <div className="container">
			    <div className="row">
			      <div className="col-lg-12">
			        <div className="section-title-area ltn__section-title-2--- text-center---">
			          <h6 className="section-subtitle section-subtitle-2--- ltn__secondary-color">Explore Neighbour</h6>
			          <h1 className="section-title">What’s In Neighbour <br />
			            Explore Below</h1>
			        </div>
			      </div>
			    </div>
			    <div className="row">
			      <div className="col-lg-12">
			        <div className="ltn__neighbour-tab-wrap">
			          <div className="ltn__tab-menu ltn__tab-menu-3--- ltn__tab-menu-4 ltn__tab-menu-top-right-- text-uppercase--- text-center">
			            <div className="nav">
			              <a className="active show" data-bs-toggle="tab" href="#liton_tab_4_1"><img src={publicUrl+"assets/img/neighbour/1.jpg"} alt="#" /></a>
			              <a data-bs-toggle="tab" href="#liton_tab_4_2" ><img src={publicUrl+"assets/img/neighbour/2.jpg"} alt="#" /></a>
			              <a data-bs-toggle="tab" href="#liton_tab_4_3" ><img src={publicUrl+"assets/img/neighbour/3.jpg"} alt="#" /></a>
			            </div>
			          </div>
			          <div className="tab-content">
			            <div className="tab-pane fade active show" id="liton_tab_4_1">
			              <div className="ltn__neighbour-tab-content-inner">
			                <div className="row">
			                  <div className="col-lg-8">
			                    <div className="neighbour-apartments-img">
			                      <img src={publicUrl+"assets/img/neighbour/1.jpg"} alt="#" />
			                    </div>
			                  </div>
			                  <div className="col-lg-4">
			                    <div className="ltn__search-by-place-item neighbour-apartments-item">
			                      <div className="search-by-place-img">
			                        <img src={publicUrl+"assets/img/product-3/3.jpg"} alt="#" />
			                        {/* <div className="search-by-place-badge">
			                          <ul>
			                            <li>9 Properties</li>
			                          </ul>
			                        </div> */}
			                      </div>
			                      <div className="search-by-place-info go-top">
			                        <h4><Link href="/product-details">Get Expert Advice</Link></h4>
			                        <label><span className="ltn__secondary-color">We are here to help you!</span></label>
			                        <div className="search-by-place-brief">
			                          <p>We will help you to choose the right property. It is our endeavor that we can get you the property of your choice with the best facilities.</p>
			                        </div>
			                        <div className="search-by-place-btn ">
			                          <Link href="tel:07550400495"><a>Make a Call Now <i className="flaticon-right-arrow" /></a></Link>
			                        </div>
			                      </div>
			                    </div>
			                  </div>
			                </div>
			              </div>
			            </div>
			            <div className="tab-pane fade" id="liton_tab_4_2">
			              <div className="ltn__neighbour-tab-content-inner">
			                <div className="row">
			                  <div className="col-lg-8">
			                    <div className="neighbour-apartments-img">
			                      <img src={publicUrl+"assets/img/neighbour/2.jpg"} alt="#" />
			                    </div>
			                  </div>
			                  <div className="col-lg-4">
			                    <div className="ltn__search-by-place-item neighbour-apartments-item">
			                      <div className="search-by-place-img">
			                        <Link href="/product-details"><img src={publicUrl+"assets/img/product-3/2.jpg"} alt="#" /></Link>
			                        <div className="search-by-place-badge">
			                          <ul>
			                            <li>9 Properties</li>
			                          </ul>
			                        </div>
			                      </div>
			                      <div className="search-by-place-info go-top">
			                        <h4><Link href="/product-details">Medical Hospital</Link></h4>
			                        <label><span className="ltn__secondary-color">1,500m </span>/ 21 min. walk</label>
			                        <div className="search-by-place-brief">
			                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing
			                            elit, sed do eiusmod tempor incididunt ut labore Enim
			                            ullamco laboris.</p>
			                        </div>
			                        <div className="search-by-place-btn ">
			                          <Link href="/product-details"><a>View Property <i className="flaticon-right-arrow" /></a></Link>
			                        </div>
			                      </div>
			                    </div>
			                  </div>
			                </div>
			              </div>
			            </div>
			            <div className="tab-pane fade" id="liton_tab_4_3">
			              <div className="ltn__neighbour-tab-content-inner">
			                <div className="row">
			                  <div className="col-lg-8">
			                    <div className="neighbour-apartments-img">
			                      <img src={publicUrl+"assets/img/neighbour/3.jpg"} alt="#" />
			                    </div>
			                  </div>
			                  <div className="col-lg-4">
			                    <div className="ltn__search-by-place-item neighbour-apartments-item">
			                      <div className="search-by-place-img">
			                        <Link href="/product-details"><img src={publicUrl+"assets/img/product-3/4.jpg"} alt="#" /></Link>
			                        <div className="search-by-place-badge">
			                          <ul>
			                            <li>9 Properties</li>
			                          </ul>
			                        </div>
			                      </div>
			                      <div className="search-by-place-info go-top">
			                        <h4><Link href="/product-details">Children Playland</Link></h4>
			                        <label><span className="ltn__secondary-color">1,500m </span>/ 21 min. walk</label>
			                        <div className="search-by-place-brief">
			                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing
			                            elit, sed do eiusmod tempor incididunt ut labore Enim
			                            ullamco laboris.</p>
			                        </div>
			                        <div className="search-by-place-btn ">
			                          <Link href="/product-details"><a>View Property <i className="flaticon-right-arrow" /></a></Link>
			                        </div>
			                      </div>
			                    </div>
			                  </div>
			                </div>
			              </div>
			            </div>
			          </div>
			        </div>
			        <div className="ltn__faq-inner ltn__faq-inner-2 ltn__faq-inner-3">
			          <div className="row">
			            <div className="col-lg-6">
			              <div id="accordion_2">
			                {/* card */}
			                <div className="card">
			                  <h6 className="collapsed ltn__card-title" data-bs-toggle="collapse" data-bs-target="#faq-item-2-1" aria-expanded="false">
			                    <i className="flaticon-mortarboard" /> University / College
			                  </h6>
			                  <div id="faq-item-2-1" className="collapse" data-bs-parent="#accordion_2">
			                    <div className="card-body">
			                      <p>We have schools, colleges and universities located near the projects for you and your family. There is so many private/govt. world class universities and convent schools.</p>
			                    </div>
			                  </div>
			                </div>
			                {/* card */}
			                <div className="card">
			                  <h6 className="collapsed ltn__card-title" data-bs-toggle="collapse" data-bs-target="#faq-item-2-2" aria-expanded="false"> 
			                    <i className="flaticon-hospital" /> Medical Hospital
			                  </h6>
			                  <div id="faq-item-2-2" className="collapse show---" data-bs-parent="#accordion_2">
			                    <div className="card-body">
			                      <p>Get quick access to top quality medical care in the area. Well-equipped hospitals, clinics, and emergency services are also available in the area.</p>
			                    </div>
			                  </div>
			                </div>                          
			                {/* card */}
			                <div className="card">
			                  <h6 className="collapsed ltn__card-title" data-bs-toggle="collapse" data-bs-target="#faq-item-2-3" aria-expanded="false">
			                    <i className="flaticon-metro" /> Railway Station/Convence
			                  </h6>
			                  <div id="faq-item-2-3" className="collapse" data-bs-parent="#accordion_2">
			                    <div className="card-body">
			                      <p>Easy commute to work, school and shopping. The projects are within walking distance to the railway station, bus stop and has easy access to the other public transportation.</p> 
			                    </div>
			                  </div>
			                </div>
			                {/*  */}
			              </div>                                
			            </div>
			            <div className="col-lg-6">
			              <div id="accordion_3">
			                {/* card */}
			                <div className="card">
			                  <h6 className="collapsed ltn__card-title" data-bs-toggle="collapse" data-bs-target="#faq-item-3-4" aria-expanded="false">
			                    <i className="flaticon-building" /> Markets
			                  </h6>
			                  <div id="faq-item-3-4" className="collapse" data-bs-parent="#accordion_3">
			                    <div className="card-body">
			                      <p>Markets are available in commercial zone of the every project. People can also go outside of the township because easy access to markets and shopping malls.</p>
			                    </div>
			                  </div>
			                </div>
			                {/* card */}
			                <div className="card">
			                  <h6 className="collapsed ltn__card-title" data-bs-toggle="collapse" data-bs-target="#faq-item-3-5" aria-expanded="false">
			                    <i className="flaticon-airplane" /> Temples / Others
			                  </h6>
			                  <div id="faq-item-3-5" className="collapse" data-bs-parent="#accordion_3">
			                    <div className="card-body">
			                      <p>People around the world come here to visit Lord Shri Krishna&#39;s birthplace. It is a holy place. Our Few projects are coming into circumambulation route, which is 84 miles long. It is a world tourist place.</p>
			                    </div>
			                  </div>
			                </div>
			                {/* card */}
			                <div className="card">
			                  <h6 className="collapsed ltn__card-title" data-bs-toggle="collapse" data-bs-target="#faq-item-3-6" aria-expanded="false">
			                    <i className="flaticon-slider" /> Parks / Children Playland
			                  </h6>
			                  <div id="faq-item-3-6" className="collapse" data-bs-parent="#accordion_3">
			                    <div className="card-body">
			                      <p>In every projects we have built park as possible as much. We blieve that a peacefull life only comes from nature and a good health. Children can enjoy the swings that have been set up for them to play in.</p>
			                    </div>
			                  </div>
			                </div>
			                {/*  */}
			              </div>                                
			            </div>
			          </div>
			        </div>
			      </div>
			    </div>
			  </div>
			</div>
        }
}

export default Neighbour