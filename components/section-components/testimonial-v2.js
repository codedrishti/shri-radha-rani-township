import React, { Component } from 'react';
import Link from 'next/link';
import parse from 'html-react-parser';

class TestimonialV2 extends Component {

    render() {

        let publicUrl = '/'
        let imagealt = 'image'

    return <div className="ltn__testimonial-area section-bg-1--- bg-image-top pt-115 pb-65" data-bs-bg={publicUrl+"assets/img/bg/23.jpg"}>
			  <div className="container">
			    <div className="row">
			      <div className="col-lg-12">
			        <div className="section-title-area ltn__section-title-2--- text-center---">
			          <h6 className="section-subtitle section-subtitle-2--- ltn__secondary-color--- white-color">Client,s Testimonial</h6>
			          <h1 className="section-title white-color">See What,s Our Client <br />
			            Says About Us</h1>
			        </div>
			      </div>
			    </div>
			    <div className="row ltn__testimonial-slider-6-active slick-arrow-3">
			      <div className="col-lg-4">
			        <div className="ltn__testimonial-item ltn__testimonial-item-7 ltn__testimonial-item-8">
			          <div className="ltn__testimoni-info">
			            <div className="ltn__testimoni-author-ratting">
			              <div className="ltn__testimoni-info-inner">
			                <div className="ltn__testimoni-img">
			                  <img src={publicUrl+"assets/img/testimonial/1.jpg"} alt="#" />
			                </div>
			                <div className="ltn__testimoni-name-designation">
			                  <h5>Unnat Awasthi</h5>
			                  <label>Lucknow, U.P</label>
			                </div>
			              </div>
			              <div className="ltn__testimoni-rating">
			                <div className="product-ratting">
			                  <ul>
			                    <li><a href="#"><i className="fas fa-star" /></a></li>
			                    <li><a href="#"><i className="fas fa-star" /></a></li>
			                    <li><a href="#"><i className="fas fa-star" /></a></li>
			                    <li><a href="#"><i className="fas fa-star" /></a></li>
			                    <li><a href="#"><i className="fas fa-star" /></a></li>
			                  </ul>
			                </div>
			              </div>
			            </div>
			            <p> 
					  I enjoyed the customer service experience at KRS Group. It was quick, easy, and painless. I felt the value for what I paid for was very good for the location. It was easy to reach there and the township is beautifully designed to develop. They were incredibly responsive and helped me to find a property that I loved. I would definitely recommend them to anyone looking for a place to invest.</p>
			          </div>
			        </div>
			      </div>
			      <div className="col-lg-4">
			        <div className="ltn__testimonial-item ltn__testimonial-item-7 ltn__testimonial-item-8">
			          <div className="ltn__testimoni-info">
			            <div className="ltn__testimoni-author-ratting">
			              <div className="ltn__testimoni-info-inner">
			                <div className="ltn__testimoni-img">
			                  <img src={publicUrl+"assets/img/testimonial/2.jpg"} alt="#" />
			                </div>
			                <div className="ltn__testimoni-name-designation">
			                  <h5>Rajat Kumar</h5>
			                  <label>New Delhi, Delhi</label>
			                </div>
			              </div>
			              <div className="ltn__testimoni-rating">
			                <div className="product-ratting">
			                  <ul>
			                    <li><a href="#"><i className="fas fa-star" /></a></li>
			                    <li><a href="#"><i className="fas fa-star" /></a></li>
			                    <li><a href="#"><i className="fas fa-star" /></a></li>
			                    <li><a href="#"><i className="fas fa-star" /></a></li>
			                    <li><a href="#"><i className="fas fa-star" /></a></li>
			                  </ul>
			                </div>
			              </div>
			            </div>
			            <p> 
					  We decided to build a house in this gated community because we were looking for a safe place to live with a lot of luxurious amenities. The design was perfect for the family and we had a great experience working with the builder. We love that KRS Group provides a community that is quiet and peaceful. The residents here are also very friendly and social. We love that there is a lot of green space.</p>
			          </div>
			        </div>
			      </div>
			      <div className="col-lg-4">
			        <div className="ltn__testimonial-item ltn__testimonial-item-7 ltn__testimonial-item-8">
			          <div className="ltn__testimoni-info">
			            <div className="ltn__testimoni-author-ratting">
			              <div className="ltn__testimoni-info-inner">
			                <div className="ltn__testimoni-img">
			                  <img src={publicUrl+"assets/img/testimonial/3.jpg"} alt="#" />
			                </div>
			                <div className="ltn__testimoni-name-designation">
			                  <h5>Vikram Sharma</h5>
			                  <label>Pune, Maharashtra</label>
			                </div>
			              </div>
			              <div className="ltn__testimoni-rating">
			                <div className="product-ratting">
			                  <ul>
			                    <li><a href="#"><i className="fas fa-star" /></a></li>
			                    <li><a href="#"><i className="fas fa-star" /></a></li>
			                    <li><a href="#"><i className="fas fa-star" /></a></li>
			                    <li><a href="#"><i className="fas fa-star" /></a></li>
			                    <li><a href="#"><i className="fas fa-star" /></a></li>
			                  </ul>
			                </div>
			              </div>
			            </div>
			            <p> 
					  This is a great investment property because it has such a great location and is such a great property. The price is affordable and the location is very convenient. It is a great investment opportunity and is a one-of-a-kind property. The property is a perfect fit for someone looking to invest in a great property with a great future and it is a convenient location.</p>
			          </div>
			        </div>
			      </div>
			      <div className="col-lg-4">
			        <div className="ltn__testimonial-item ltn__testimonial-item-7 ltn__testimonial-item-8">
			          <div className="ltn__testimoni-info">
			            <div className="ltn__testimoni-author-ratting">
			              <div className="ltn__testimoni-info-inner">
			                <div className="ltn__testimoni-img">
			                  <img src={publicUrl+"assets/img/testimonial/4.jpg"} alt="#" />
			                </div>
			                <div className="ltn__testimoni-name-designation">
			                  <h5>Gargee Mondal</h5>
			                  <label>Banglore, Karnataka</label>
			                </div>
			              </div>
			              <div className="ltn__testimoni-rating">
			                <div className="product-ratting">
			                  <ul>
			                    <li><a href="#"><i className="fas fa-star" /></a></li>
			                    <li><a href="#"><i className="fas fa-star" /></a></li>
			                    <li><a href="#"><i className="fas fa-star" /></a></li>
			                    <li><a href="#"><i className="fas fa-star" /></a></li>
			                    <li><a href="#"><i className="fas fa-star" /></a></li>
			                  </ul>
			                </div>
			              </div>
			            </div>
			            <p> 
					  This is an amazing property. The KRS Group has done an amazing job of building the gated township and making it available for construction. This is a great opportunity for anyone interested in living in the Holy Land of Lord Krishna. I can say that township has all those aminities that are required to live a luxurious life. They are doing great job, thanks and keep it up guys.</p>
			          </div>
			        </div>
			      </div>
			      {/*  */}
			    </div>
			  </div>
			</div>

        }
}

export default TestimonialV2