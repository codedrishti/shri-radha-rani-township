import React, { Component } from "react";
import Link from "next/link";
import parse from "html-react-parser";

class FeaturesV1 extends Component {
  render() {
    let publicUrl = "/";

    let customClass = this.props.customClass ? this.props.customClass : "";

    return (
      <div className={customClass}>
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="section-title-area ltn__section-title-2--- text-center">
                <h6 className="section-subtitle section-subtitle-2 ltn__secondary-color">
                  Our Services
                </h6>
                <h1 className="section-title">Our Main Focus</h1>
              </div>
            </div>
          </div>
          <div className="row ltn__custom-gutter--- justify-content-center go-top">
            <div className="col-lg-4 col-sm-6 col-12">
              <div className="ltn__feature-item ltn__feature-item-6 text-center bg-white  box-shadow-1">
                <div className="ltn__feature-icon">
                  <img
                    src={publicUrl + "assets/img/icons/icon-img/21.png"}
                    alt="#"
                  />
                </div>
                <div className="ltn__feature-info">
                  <h3>
                    <Link href="/service-details">Property Management</Link>
                  </h3>
                  <p>
                  Over 10 thousand+ homes for sale are available on our sites all over India, we can provide you with a perfect house that will match your comfort and status both..
                  </p>
                  <Link className="ltn__service-btn" href="/service-details">
                    <a>
                      Find A Home <i className="flaticon-right-arrow" />
                    </a>
                  </Link>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-sm-6 col-12">
              <div className="ltn__feature-item ltn__feature-item-6 text-center bg-white  box-shadow-1 active">
                <div className="ltn__feature-icon">
                  <img
                    src={publicUrl + "assets/img/icons/icon-img/22.png"}
                    alt="#"
                  />
                </div>
                <div className="ltn__feature-info">
                  <h3>
                    <Link href="/service-details">Real Estate </Link>
                  </h3>
                  <p>
                  In real estate,  Shri Radha Rani township is one of the leading builder brands of India. We are the leading professionals of Real estate including residential plots, villas and apartments.
                  </p>
                  <Link className="ltn__service-btn" href="/service-details">
                    <a>
                      Find A Home <i className="flaticon-right-arrow" />
                    </a>
                  </Link>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-sm-6 col-12">
              <div className="ltn__feature-item ltn__feature-item-6 text-center bg-white  box-shadow-1">
                <div className="ltn__feature-icon">
                  <img
                    src={publicUrl + "assets/img/icons/icon-img/23.png"}
                    alt="#"
                  />
                </div>
                <div className="ltn__feature-info">
                  <h3>
                    <Link href="/service-details">Commercial Space</Link>
                  </h3>
                  <p>
                  In the commercial sector, Krs Township is the leading brand with versatility, excellence and quality. Dealing in Commercial plots, shops, schools and hospitals.
                  </p>
                  <Link className="ltn__service-btn" href="/service-details">
                    <a>
                      Find A Home <i className="flaticon-right-arrow" />
                    </a>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default FeaturesV1;
