import React, { Component } from "react";
import Link from "next/link";
import parse from "html-react-parser";

class BannerV2 extends Component {
  render() {
    let publicUrl = "/";
    let imagealt = "image";

    return (
      <div className="ltn__slider-area ltn__slider-11  ltn__slider-11-slide-item-count-show--- ltn__slider-11-pagination-count-show--- section-bg-1">
        <div className="ltn__slider-11-inner">
          <div className="ltn__slider-11-active">
            {/* slide-item */}
            <div className="ltn__slide-item ltn__slide-item-2 ltn__slide-item-3-normal ltn__slide-item-3 ltn__slide-item-11">
              <div className="ltn__slide-item-inner">
                <div className="container">
                  <div className="row">
                    <div className="col-lg-12 align-self-center">
                      <div className="slide-item-info">
                        <div className="slide-item-info-inner ltn__slide-animation">
                          <div className="slide-video mb-50 d-none">
                            <a
                              className="ltn__video-icon-2 ltn__video-icon-2-border"
                              href="https://www.youtube.com/embed/tlThdr3O5Qo"
                              data-rel="lightcase:myCollection"
                            >
                              <i className="fa fa-play" />
                            </a>
                          </div>
                          <h6 className="slide-sub-title white-color--- animated">
                            <span>
                              <i className="fas fa-home" />
                            </span>{" "}
                            Real Estate Developer
                          </h6>
                          <h1 className="slide-title animated ">
                          HOME OR HEAVEN? <br />
                          YOU <span>DECIDE!</span>
                          </h1>
                          <div className="slide-brief animated">
                            <p>
                            Our township is designed with the needs of the customers in mind.We offer a wide range of features, which all make this a perfect destination for all.
                            </p>
                          </div>
                          
                          <div className="btn-wrapper animated">
                            <a
                              className="theme-btn-1 btn btn-effect-1"
                              data-bs-toggle="modal" data-bs-target="#enquiryPopup"
                            >
                              Make An Enquiry
                            </a>
                            <a
                              className="ltn__video-play-btn bg-white"
                              href="https://www.youtube.com/embed/rYh78dpxzuk?autoplay=1&showinfo=0"
                              data-rel="lightcase"
                            >
                              <i className="icon-play  ltn__secondary-color" />
                            </a>
                          </div>
                        </div>
                      </div>
                      <div className="slide-item-img">
                        <img
                          src={publicUrl + "assets/img/slider/61.jpg"}
                          alt="#"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* slide-item */}
            <div className="ltn__slide-item ltn__slide-item-2 ltn__slide-item-3-normal ltn__slide-item-3 ltn__slide-item-11">
              <div className="ltn__slide-item-inner">
                <div className="container">
                  <div className="row">
                    <div className="col-lg-12 align-self-center">
                      <div className="slide-item-info">
                        <div className="slide-item-info-inner ltn__slide-animation">
                          <div className="slide-video mb-50 d-none">
                            <a
                              className="ltn__video-icon-2 ltn__video-icon-2-border"
                              href="https://www.youtube.com/embed/tlThdr3O5Qo"
                              data-rel="lightcase:myCollection"
                            >
                              <i className="fa fa-play" />
                            </a>
                          </div>
                          <h6 className="slide-sub-title white-color--- animated">
                            <span>
                              <i className="fas fa-home" />
                            </span>{" "}
                            Real Estate Developer
                          </h6>
                          <h1 className="slide-title animated ">
                          Quality Township, <br />  Luxurious Living.
                          </h1>
                          <div className="slide-brief animated">
                            <p>
                            We are committed to providing world-class amenities and facilities to our customers. All the projects are well developed for a peaceful and joyful life. 
                            </p>
                          </div>
                          <div className="btn-wrapper animated">
                            <a
                              href="projects"
                              className="theme-btn-1 btn btn-effect-1"
                            >
                              OUR PROJECTS
                            </a>
                            <a
                              href="contact"
                              className="btn btn-transparent btn-effect-3"
                            >
                              CONTACT US
                            </a>
                          </div>
                        </div>
                      </div>
                      <div className="slide-item-img">
                        <img
                          src={publicUrl + "assets/img/slider/62.jpg"}
                          alt="#"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* slide-item */}
            <div className="ltn__slide-item ltn__slide-item-2 ltn__slide-item-3-normal ltn__slide-item-3 ltn__slide-item-11">
              <div className="ltn__slide-item-inner">
                <div className="container">
                  <div className="row">
                    <div className="col-lg-12 align-self-center">
                      <div className="slide-item-info">
                        <div className="slide-item-info-inner ltn__slide-animation">
                          <div className="slide-video mb-50 d-none">
                            <a
                              className="ltn__video-icon-2 ltn__video-icon-2-border"
                              href="https://www.youtube.com/embed/tlThdr3O5Qo"
                              data-rel="lightcase:myCollection"
                            >
                              <i className="fa fa-play" />
                            </a>
                          </div>
                          <h6 className="slide-sub-title white-color--- animated">
                            <span>
                              <i className="fas fa-home" />
                            </span>{" "}
                            Real Estate Developer
                          </h6>
                          <h1 className="slide-title animated ">
                          Make Your Dream <br /> A Reality.
                          </h1>
                          <div className="slide-brief animated">
                            <p>
                            People who live in these township live a comfortable, peaceful and joyful life. They also have easy access to all the facilities they need.
                            </p>
                          </div>
                          <div className="btn-wrapper animated">
                            <a
                              href="faq"
                              className="theme-btn-1 btn btn-effect-1"
                            >
                              FAQ
                            </a>
                            <a
                              href="contact"
                              className="btn btn-transparent btn-effect-3"
                            >
                              CONTACT US
                            </a>
                          </div>
                        </div>
                      </div>
                      <div className="slide-item-img">
                        <img
                          src={publicUrl + "assets/img/slider/63.jpg"}
                          alt="#"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* slider-4-pagination */}
          <div className="ltn__slider-11-pagination-count">
            <span className="count" />
            <span className="total" />
          </div>
          {/* slider-sticky-icon */}
          <div className="slider-sticky-icon-2">
            <ul>
              <li>
                <a href="#" title="Facebook">
                  <i className="fab fa-facebook-f" />
                </a>
              </li>
              <li>
                <a href="#" title="Twitter">
                  <i className="fab fa-twitter" />
                </a>
              </li>
              <li>
                <a href="#" title="Linkedin">
                  <i className="fab fa-linkedin" />
                </a>
              </li>
            </ul>
          </div>
          {/* slider-4-img-slide-arrow */}
          <div className="ltn__slider-11-img-slide-arrow">
            <div className="ltn__slider-11-img-slide-arrow-inner">
              <div className="ltn__slider-11-img-slide-arrow-active">
                <div className="image-slide-item">
                  <img
                    src={publicUrl + "assets/img/slider/61.jpg"}
                    alt="Flower Image"
                  />
                </div>
                <div className="image-slide-item">
                  <img
                    src={publicUrl + "assets/img/slider/62.jpg"}
                    alt="Flower Image"
                  />
                </div>
                <div className="image-slide-item">
                  <img
                    src={publicUrl + "assets/img/slider/63.jpg"}
                    alt="Flower Image"
                  />
                </div>
              </div>
              {/* slider-4-slide-item-count */}
              <div className="ltn__slider-11-slide-item-count">
                <span className="count" />
                <span className="total" />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default BannerV2;
