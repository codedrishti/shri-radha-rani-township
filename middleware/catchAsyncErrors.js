const ShowError = require("./error");

module.exports = (theFunc) => (req, res) => {
     Promise.resolve(theFunc(req, res)).catch((error)=>{
          ShowError(error, res);
     });
}

