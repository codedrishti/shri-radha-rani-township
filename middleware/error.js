const { object } = require("webidl-conversions");

module.exports = (err, res) => {
     let statusCode = err.statusCode || 500;
     let message = err.message || "Internal Server Error";


     //CastError: Wrong MongoDB ID/Data Error
     if(err && err["name"] === "CastError"){
          message = `Resource not found. Invailid: ${Object.keys(err.keyValue)}`;
     }

     //Mongoose Duplicate Key Error
     if (err && err["code"] === 11000) {
          message = `Duplicate ${Object.keys(err.keyValue)} Entered`;
     }

     //Handle JWT Error
     if(err && err["code"] === "JsonWebTokenError"){
          message = `Json web token is invailid, Try again`;
     }

     //JWT Expire Error
     if(err && err["code"] === "TokenExpiredError"){
          message = `Json web token is Expired, Try again`;
     }

     res.status(statusCode).json({
          success: false,
          message: message,
     });
};
