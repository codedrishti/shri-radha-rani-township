/** @type {import('next').NextConfig} */
fs = require("fs");
const nextConfig = {
  reactStrictMode: true,
  env: {
    rawJsFromFile: fs.readFileSync("./assets/js/plugins.js").toString(),
    rawJsFromFileMain: fs.readFileSync("./assets/js/main.js").toString(),
    MONGO_URI: "mongodb+srv://srrtownship:Cloud2412@township.eyplp.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
    SERVER_URL: "http://localhost:3000",
  },
  eslint: {
    // Warning: This allows production builds to successfully complete even if
    // your project has ESLint errors.
    ignoreDuringBuilds: true,
  },
 
};

module.exports = nextConfig;
