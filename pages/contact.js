import React from 'react';
import Navbar from '../components/global-components/navbar-v2';
import PageHeader from '../components/global-components/page-header';
import ContactInfo from '../components/section-components/contact-info';
import ContactForm from '../components/section-components/contact-form';
import Map from '../components/section-components/map';
import CallToActionV1 from '../components/section-components/call-to-action-v1';
import Footer from '../components/global-components/footer';
import SEO from '../components/seo';

const ContactV1 = () => {
    return <div>

        <SEO title='Find Shriradharanitownship.com & KRS Group Office Address & Contact Details Across India' description='Get Shriradharanitownship.com or krsgroup contact detail and office address across India. Find the property in gated community with all aminities.' />

        <Navbar />
        <PageHeader headertitle="Contact Us" subheader="Contact" />
        <ContactInfo />
        <ContactForm />
        <Map />
        <CallToActionV1 />
        <Footer />
    </div>
}

export default ContactV1

