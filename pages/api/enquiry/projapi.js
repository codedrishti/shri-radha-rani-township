require("../../../utils/dbConnect");
import {createProjEnquiry} from "../../../controller/enquiryController";

export default async function handler(req, res) {
     const {
          method,
     } = req;

     switch (method) {
          case "POST":
               createProjEnquiry(req, res);
               break;
          default:
               res.status(400).json({ success: false });
               break;
     }
};
