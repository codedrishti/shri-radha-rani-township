require("../../../utils/dbConnect");
import {createEnquiry} from "../../../controller/enquiryController";

export default async function handler(req, res) {
     const {
          method,
     } = req;

     console.log(method);

     switch (method) {
          case "POST":
               createEnquiry(req, res);
               break;
          default:
               res.status(400).json({ success: false });
               break;
     }
};
