require("../../../utils/dbConnect");
import {createContact} from "../../../controller/contactController";

export default async function handler(req, res) {
     const {
          method,
     } = req;

     switch (method) {
          case "POST":
               createContact(req, res);
               break;
          default:
               res.status(400).json({ success: false });
               break;
     }
};
