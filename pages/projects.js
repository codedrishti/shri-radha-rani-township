import React from 'react';
import Navbar from '../components/global-components/navbar-v2';
import PageHeader from '../components/global-components/page-header';
import ShogGrid from '../components/shop-components/shop-grid-v1';
import CallToActionV1 from '../components/section-components/call-to-action-v1';
import Footer from '../components/global-components/footer';
import SEO from '../components/seo';

const ShopGrid_V1 = () => {
    return <div>

        <SEO title='Know About Shri Radha Rani Township Projects Residential and commercial plots are available at affordable prices.' description='Looking for affordable plots in the best cities in the country? Shriradharanitownship.com offers the best plots in Mathura, Agra, Govardhan - India. Here you can check our all projects List, We are offering residential and commercial plots at affordable cost.' />

        <Navbar />
        <PageHeader headertitle="Shop Grid" />
        <ShogGrid />
        <CallToActionV1 />
        <Footer />
    </div>
}

export default ShopGrid_V1

