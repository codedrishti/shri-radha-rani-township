import React from "react";
import NavbarV2 from "../components/global-components/navbar-v2";
import PageHeader from "../components/global-components/page-header";
import Faq from "../components/section-components/faq-v1";
import Counter from "../components/section-components/counter-v1";
import BlogSlider from "../components/blog-components/blog-slider-v1";
import CallToActionV1 from "../components/section-components/call-to-action-v1";
import Footer from "../components/global-components/footer";

export default function faq() {
     return (
          <>
               <NavbarV2 />
               <PageHeader headertitle="Frequently asked questions" subheader="FAQ" />
               <Faq />
               <CallToActionV1 />
               <Footer />
          </>
     );
}
