import React, { useState, useEffect } from "react";
import Navbar from "../components/global-components/navbar-v2";
import PageHeader from "../components/global-components/page-header";
import ProductSlider from "../components/shop-components/product-slider-v1";
import ProductDetails from "../components/shop-components/shop-details";
import CallToActionV1 from "../components/section-components/call-to-action-v1";
import Footer from "../components/global-components/footer";
import SEO from "../components/seo";

const allProjectData = require("../controller/ProjectDetails.json");

export const getStaticPaths = async() => {
     // const res = await fetch("http://localhost:3000/api/blog");
     // const {blogs} = await res.json();
     
     const paths = allProjectData.map((curElem) => {
          return {
               params: {
                    projectDetails: curElem.permaLink,
               }
          }
     });

     return{
          paths,
          fallback: false,
     }
}

export const getStaticProps = async(context) => {
     // const res = await fetch(`http://localhost:3000/api/blog/${context.params.articlePage}`);
     // const posts = await res.json();

     const projectData = allProjectData.filter(
          (projData) => projData.permaLink === context.params.projectDetails
     );

     return {
          props : {
               project: projectData[0]
          }
     }
}

const Project_Details = ({project}) => {
     
     return (
          <div>
               <SEO title={project.seo.title} description={project.seo.description} />
               <Navbar />
               <PageHeader headertitle={project.projectName} sublink={project.permaLink} customclass="mb-0 pd-0" />
               
               <ProductSlider projectSlider={project.slider} />

               <ProductDetails projectDetails={project} />
               <CallToActionV1 />
               <Footer />
          </div>
     );
};

export default Project_Details;
