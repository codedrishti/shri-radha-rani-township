import React from 'react';
import Navbar from "../components/global-components/navbar-v2";
import PageHeader from '../components/global-components/page-header';
import AboutV4 from '../components/section-components/about-v4';
import Features from '../components/section-components/features-v1';
import Team from '../components/section-components/team-v1';
import Testimonial from '../components/section-components/testimonial-v1';
import BlogSlider from '../components/blog-components/blog-slider-v1';
import CallToActionV1 from '../components/section-components/call-to-action-v1';
import Footer from '../components/global-components/footer';
import SEO from '../components/seo';


const About = () => {
    return <div>

        <SEO title='About Us - Shriradharanitownship.com' description='We are developing societies since 20 Years. Shri Radha Rani Township is a chain of townships and these society are developed by KRS Group & its companies. We are leading real-estate developer in Mathura, Vrindavan, Barsana, Govardhan, Agra - India'/>

        <Navbar />
        <PageHeader headertitle="About Us" />
        <AboutV4 />
        <Features customClass="ltn__feature-area section-bg-1 pt-120 pb-90 mb-120---" />
        {/* <Testimonial /> */}
        {/* <BlogSlider /> */}
        <CallToActionV1 />
        <Footer />
    </div>
}

export default About

