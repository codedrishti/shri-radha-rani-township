require("../utils/dbConnect")
const ErrorHandler = require("../utils/errorhandler");
const catchAsyncErrors = require("../middleware/catchAsyncErrors");
const Contact = require("../models/contactModels");

exports.createContact = catchAsyncErrors(async(req, res) => {

     req.body.subject = "New Contact From Shri Radha Rani Website";
     
     const contact = await Contact.create(req.body);
     res.status(201).json({success: true, contact});
     
});