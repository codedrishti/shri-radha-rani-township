require("../utils/dbConnect")
const ErrorHandler = require("../utils/errorhandler");
const catchAsyncErrors = require("../middleware/catchAsyncErrors");
const Enquiry = require("../models/enquiryModels");
const ProjEnquiry = require("../models/projEnquiryModels");

exports.createEnquiry = catchAsyncErrors(async(req, res) => {

     req.body.address != "" ? req.body.subject = "Become An Associate!" : req.body.subject = "New Enquiry - SRRT";
     
     const enquiry = await Enquiry.create(req.body);
     res.status(201).json({success: true, enquiry});
});

exports.createProjEnquiry = catchAsyncErrors(async(req, res) => {
     req.body.subject = "New Enquiry - SRRT";
     const enquiry = await ProjEnquiry.create(req.body);
     res.status(201).json({success: true, enquiry});
});