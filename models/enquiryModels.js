import mongoose from "mongoose";
import validator from "validator";

const enquirySchema = new mongoose.Schema({
     subject: {
          type: String,
     },
     name: {
          type: String,
          required: true,
     },
     email: {
          type: String,
          required: true,
     },
     phone: {
          type: Number,
          required: true,
     },
     address: {
          type: String,
     },
}, {timestamps: true});

module.exports = mongoose.models.Enquiry || mongoose.model('Enquiry', enquirySchema);