import mongoose from "mongoose";
import validator from "validator";

const contactSchema = new mongoose.Schema({
     subject: {
          type: String,
     },
     name: {
          type: String,
          required: true,
     },
     email: {
          type: String,
          required: true,
     },
     phone: {
          type: Number,
          required: true,
     },
     project: {
          type: String,
     },
     message: {
          type: String,
     }
}, {timestamps: true});

module.exports = mongoose.models.Contact || mongoose.model('Contact', contactSchema);