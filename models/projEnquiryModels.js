import mongoose from "mongoose";
import validator from "validator";

const projEnquirySchema = new mongoose.Schema({
     subject: {
          type: String,
     },
     name: {
          type: String,
          required: true,
     },
     email: {
          type: String,
          required: true,
     },
     phone: {
          type: Number,
          required: true,
     },
     message: {
          type: String,
     }
}, {timestamps: true});

module.exports = mongoose.models.ProjectEnquiry || mongoose.model('ProjectEnquiry', projEnquirySchema);